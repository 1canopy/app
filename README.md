# Test Automation Framework
A simple test automation framework which provides the following features:
###### Short Description about the **Framework**
- TestNG based framework
- Use `@NeedBrowser` annotation on Test method for automatic driver management
- Or, use `browserName` parameter in the TestNG suite xml to manage browser driver initiations
- Out of the box parallel execution ready framework
- Using [SimpleSe](https://github.com/RationaleEmotions/SimpleSe) to auto manage the page objects from the simple json files
- Integrated with [TestNG Extent Reporter](https://github.com/email2vimalraj/TestNGExtentsReport)

### Manage test data
- The test data is maintained in properties file for every environment under `src/test/resources/data/<ENV>/` directory. Here `ENV` is nothing but your environment name which you pass during execution.
For example:
```
src
    test
        resources
            data
                QA1
                    client.properties
                    user.properties
                DEVINT
                    client.properties
                    user.properties
```

- All the properties based on the environment will be loaded and available at any point in your execution.
- To retrieve a particular test data, you can write `TestDataRetriever.getTestData("userName");`. Here, `userName` is a property key from any of the property files.
- The test execution report will showcase the used test data.

### Collecting Test Evidence
- At any point of time, you can capture the screenshot and attach a description to it to make the screenshot as an evidence using: `DriverCommand.takeScreenshot("Navigated to checkbox page");`
- All the collected screenshots are put into a PDF document as a Test Evidence document for each test method and attached as part of the report file.

#### Maven Runtime Execution parameters
- mvn clean test -DDefaultENV=QA2 -Dapplication.name=eConnect -Drelease.version=18.3 -Dtest.suite.filename="TestNG_SmokeSuite.xml"
