### Change log
##### Manage environments
- You can set a default environment in the `pom.xml` under `maven-surefire-plugin` build plugin system properties. For example:
```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-surefire-plugin</artifactId>
    <version>2.20.1</version>
    <configuration>
        <suiteXmlFiles>
            <file>src/test/resources/suites/HerokuAppSuite.xml</file>
        </suiteXmlFiles>
        <systemPropertyVariables>
            <env>DEV</env>
        </systemPropertyVariables>
    </configuration>
</plugin>
```

The above snippet sets the default environment to `DEV`.

- At the time of execution you can override the environment value as simple as passing maven parameters, like: `mvn clean test -Denv=QA`. Now the environment is overridden from DEV to QA.


## Developer Notes

- Created `@SVBFindBy` annotation which is pretty much similar to selenium's `@FindBy`, however we've added two new parameters `until` and `timeout`.
- `until` is of type `Until`, an enum, currently we are supporting `AVAILABLE`, `VISIBLE` and `CLICKABLE`.
- Any web element field can be annotated with `@SVBFindBy`.
- Use `CustomPageFactory.initElements()` method to initiate the page factory.
- From anywhere, you can call `DriverManager.getDriver()` method to get the created driver instance.
- You could create driver instance by supplying either a `browserName` for local execution mostly or `remoteUrl` and `capabilityFilePath` for remote execution. The `capabilityFilePath` is a properties file and relative to your `test\resources` directory.
- After the execution, the reports are generated at the `test-output` directory. There will be two reports generated, one is detailed report and the other one is high-level report which can be shared through an email.