package com.svb.bcc.smoke;

import com.svb.annotations.NeedBrowser;
import com.svb.constants.TestGroupTags;
import com.svb.core.EnvironmentUtils;
import com.svb.core.TestDataRetriever;
import com.svb.drivers.BasePageSetup;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import com.svb.pages.ConfirmIdentityModal;
import com.svb.pages.HomePage;
import com.svb.pages.IdentityConfirmedModal;
import com.svb.pages.LoginPage;
import com.svb.pages.cards.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.Test;

/**
 * Created by ashetty on 04/12/2018
 */

public class BccCardsNewCardRequestTest extends BasePageSetup {

    private static Logger logger = LogManager.getLogger(com.svb.econnect.account.AccountReportingAddQueryTest.class);

    private RemoteWebDriver driver;
    private LoginPage loginPage;
    private String currentTestCaseName;
    private CardAdminHomePage cardAdminHomePage;
    private CardAccountOverviewPage cardAccountOverviewPage;
    private String webContentValidation;
    private OpenNewCardPage openNewCardPage;
    private OpenNewCardVerificationPage openNewCardVerificationPage;
    private OpenNewCardConfirmationPage openNewCardConfirmationPage;

    /* @BeforeTest(alwaysRun = true)
     public void initiateSetup() {

     }*/
    @NeedBrowser
    @Test(groups = {TestGroupTags.SMOKE})
    public void bccNewCardRequestCheckPlastic(final ITestContext testContext) throws InterruptedException {
        currentTestCaseName = testContext.getName();
        bccNewCardRequestCheckCardType(currentTestCaseName, "Plastic");
    }

    @NeedBrowser
//    @Test(groups = {TestGroupTags.SMOKE})
    @Test(invocationCount = 400)
    public void bccNewCardRequestCheckNonPlastic(final ITestContext testContext) throws InterruptedException {
        currentTestCaseName = testContext.getName();
        bccNewCardRequestCheckCardType(currentTestCaseName, "Non Plastic");
    }

    private void bccNewCardRequestCheckCardType(String currentTestCaseName, String cardType) throws InterruptedException {
        DriverCommand.setThreadTestName(currentTestCaseName);

        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(driver, this);

        driver.get(EnvironmentUtils.getUrl());
        //driver.manage().window().maximize();

        loginPage = new LoginPage();
        ConfirmIdentityModal confirmIdentityModal = loginPage.login(TestDataRetriever.getTestData("client.bcc.user.name").toString(), TestDataRetriever.getTestData("client.bcc.user.pwd").toString());
        Assert.assertTrue(confirmIdentityModal.isIdentityModalDisplayed());

        IdentityConfirmedModal identityConfirmedModal = confirmIdentityModal.handleConfirmIdentityModal();
        Assert.assertNotNull(identityConfirmedModal);
        Assert.assertTrue(identityConfirmedModal.isIdentityConfirmedModalDisplayed());

        DriverCommand.takeScreenshot("Identity Confirmed - Continue to Online Banking - Test Evidence");
        HomePage homePage = identityConfirmedModal.handleIdentityConfirmed();
        //Assert.assertTrue(homePage.isHomePageDisplayed());

        DriverCommand.takeScreenshot("Logged-In User's HomePage is displayed");

//        homePage.isDownloadTrusteerPopUpDisplayed();

        //BCC New Card Request
        cardAdminHomePage = new CardAdminHomePage();
        cardAdminHomePage.navigateToCardAccountOverviewPage();
        cardAccountOverviewPage = new CardAccountOverviewPage();
        cardAccountOverviewPage.clickOpenNewCardLink();
        openNewCardPage = new OpenNewCardPage();
        DriverCommand.takeScreenshot("Open New Card Request Page - Test Evidence");
        openNewCardPage.fillNewCardDetails(cardType, "Standard", "No");
        DriverCommand.takeScreenshot("Open New Card Request Page Details Filled - Test Evidence");
        openNewCardVerificationPage = new OpenNewCardVerificationPage();
        DriverCommand.takeScreenshot("Open New Card Request Verification Page - Test Evidence");
        openNewCardVerificationPage.submitVerification();
        openNewCardConfirmationPage = new OpenNewCardConfirmationPage();
        DriverCommand.takeScreenshot("Open New Card Request Confirmation Page - Test Evidence");
        openNewCardConfirmationPage.verifyConfirmation();

        //Logout the current user session
        cardAdminHomePage.logoutSession();
        DriverCommand.takeScreenshot("Logout the user session successfully", currentTestCaseName);
    }

}
