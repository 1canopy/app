package com.svb.bcc.smoke;

import com.svb.annotations.NeedBrowser;
import com.svb.constants.TestGroupTags;
import com.svb.core.EnvironmentUtils;
import com.svb.core.TestDataRetriever;
import com.svb.drivers.BasePageSetup;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import com.svb.pages.ConfirmIdentityModal;
import com.svb.pages.HomePage;
import com.svb.pages.IdentityConfirmedModal;
import com.svb.pages.LoginPage;
import com.svb.pages.cards.BccProductDetailsPage;
import com.svb.pages.cards.CardAdminHomePage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import java.util.ArrayList;

/**
 * Created by ashetty on 04/12/2018
 */

public class BccProductDetailsPageContentCheckTest extends BasePageSetup {

    private static Logger logger = LogManager.getLogger(com.svb.econnect.account.AccountReportingAddQueryTest.class);

    private RemoteWebDriver driver;
    private LoginPage loginPage;
    private CardAdminHomePage cardAdminHomePage;
    private BccProductDetailsPage bccProductDetailsPage;
    private String currentTestCaseName;
    private String webContentValidation;

   /* @BeforeTest(alwaysRun = true)
    public void initiateSetup() {

    }*/

   @NeedBrowser
   @Test(groups = {TestGroupTags.SMOKE})
   public void bccOfferDisplayPageCheck(final ITestContext testContext) {

       currentTestCaseName = testContext.getName();
       DriverCommand.setThreadTestName(currentTestCaseName);

       driver = DriverManager.getDriver();
       CustomPageFactory.initElements(driver, this);

       driver.get(EnvironmentUtils.getUrl());
       //driver.manage().window().maximize();

       loginPage = new LoginPage();
       ConfirmIdentityModal confirmIdentityModal = loginPage.login(TestDataRetriever.getTestData("client.bcc.user.name").toString(), TestDataRetriever.getTestData("client.bcc.user.pwd").toString());
       Assert.assertTrue(confirmIdentityModal.isIdentityModalDisplayed());

       IdentityConfirmedModal identityConfirmedModal = confirmIdentityModal.handleConfirmIdentityModal();
       Assert.assertNotNull(identityConfirmedModal);
       Assert.assertTrue(identityConfirmedModal.isIdentityConfirmedModalDisplayed());

       DriverCommand.takeScreenshot("Identity Confirmed - Continue to Online Banking - Test Evidence");
       HomePage homePage = identityConfirmedModal.handleIdentityConfirmed();
       //Assert.assertTrue(homePage.isHomePageDisplayed());

       DriverCommand.takeScreenshot("Logged-In User's HomePage is displayed");

       homePage.isDownloadTrusteerPopUpDisplayed();

       //Navigate to Product Offer Details Page
       cardAdminHomePage = new CardAdminHomePage();
       cardAdminHomePage.bccOfferDisplayedScreenshot();
       cardAdminHomePage.clickLearnMoreButton();

       //Check Product Offer Details Content
       bccProductDetailsPage = new BccProductDetailsPage();
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(0),"SVB INNOVATORS CARD PROGRAM");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(1),"A Card Designed for Growing Companies");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(2),"YOU NEED TO FOCUS ON BUILDING, NOT BANKING. CONSIDER IT DONE.");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(3),"We've pre-qualified Cangrade, Inc. for an SVB Innovators Card program. Both you and your team can enjoy these benefits.");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(4),"NO PERSONAL LIABILITY");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(5),"Protect your personal credit without limiting your company's capital.");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(6),"CUSTOM CREDIT LIMIT");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(7),"Credit that scales with your growth.1");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(8),"NO INTEREST");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(9),"Because you pay the full balance each month.2");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(10),"COMPLETE YOUR APPLICATION TODAY");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(11),"Business Legal Name");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(12),"Cangrade, Inc.");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(13),"Applicant Name");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(14),"Taylor Allison");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(15),"Applicant Contact Email");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(16),"ataylor12@svb.com");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(17),"If you have questions about SVB's card products, please contact your SVB representative.");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(18),"Terms and Conditions");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(19),"APPLY NOW");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(20),"Sync with QuickBooks, Expensify, and More");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(21),"Automatically sync your card data with SVB's range of connected apps.");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(22),"Combine Your Banking");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(23),"SVB Online Banking combines your SVB card and bank accounts for a single view.");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(24),"Reward Your Business");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(25),"Earn 1 point per $1 spent and redeem for statement credits, gift cards, travel, and more.");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(26),"Priced Right");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(27),"No membership fee for your first year ($49 per year after that), no rewards fee.3");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(28),"1. A cash collateral account may be required to secure the card.");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(29),"2. Automatic payment enrollment may be required as a term of credit approval by using a designated SVB deposit account. Refer to the program terms and conditions for more information.");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(30),"3. Additional fees include: Cash Advance -3% of each Cash Advance amount, but not less than $3 or more than $50; Foreign Currency Transaction -2% of the transaction amount; Late Payment -$32 or 2.5% of the charges that have been billed on the periodic statement and remain unpaid for one or more billing cycles, whichever is greater; Return Payment $29; Rush or Expedited Card -$35. For more information about fees, please refer to the Terms and Conditions. Card benefits are provided by Mastercard. Some restrictions apply. Mastercard is a registered trademark of Mastercard Worldwide.");
       Assert.assertEquals(bccProductDetailsPage.checkBccProductDetailsDisplay().get(31),"© 2018 SVB Financial Group. All rights reserved. SVB, SVB FINANCIAL GROUP, SILICON VALLEY BANK, MAKE NEXT HAPPEN NOW and the chevron device are trademarks of SVB Financial Group, used under license. Silicon Valley Bank is a member of the FDIC and the Federal Reserve System. Silicon Valley Bank is the California bank subsidiary of SVB Financial Group (Nasdaq: SIVB).");
       DriverCommand.takeScreenshot("Test Evidence: BCC Product Details Page Content");
       JavascriptExecutor jse = (JavascriptExecutor)driver;
       jse.executeScript("scroll(0, 750);");
       DriverCommand.takeScreenshot("Test Evidence: BCC Product Details Page Content 2");
       //Logout the current user session
       bccProductDetailsPage.logoutSession();
       DriverCommand.takeScreenshot("Logout the user session successfully", currentTestCaseName);
       }

}
