package com.svb.bcc.smoke;

import com.svb.annotations.NeedBrowser;
import com.svb.constants.AccountReportingEnum;
import com.svb.constants.TestGroupTags;
import com.svb.core.EnvironmentUtils;
import com.svb.core.TestDataRetriever;
import com.svb.drivers.BasePageSetup;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import com.svb.pages.*;
import com.svb.pages.cards.CardAccountOverviewPage;
import com.svb.pages.cards.CardAdminHomePage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.Test;

/**
 * Created by ashetty on 04/12/2018
 */

public class BccCardsOverviewPageCheckTest extends BasePageSetup {

    private static Logger logger = LogManager.getLogger(com.svb.econnect.account.AccountReportingAddQueryTest.class);

    private RemoteWebDriver driver;
    private LoginPage loginPage;
    private CardAdminHomePage cardAdminHomePage;
    private String currentTestCaseName;
    private String webContentValidation;

   /* @BeforeTest(alwaysRun = true)
    public void initiateSetup() {

    }*/

   @NeedBrowser
   @Test(groups = {TestGroupTags.SMOKE})
   public void bccOfferDisplayPageCheck(final ITestContext testContext) {

       currentTestCaseName = testContext.getName();
       DriverCommand.setThreadTestName(currentTestCaseName);

       driver = DriverManager.getDriver();
       CustomPageFactory.initElements(driver, this);

       driver.get(EnvironmentUtils.getUrl());
       //driver.manage().window().maximize();

       loginPage = new LoginPage();
       ConfirmIdentityModal confirmIdentityModal = loginPage.login(TestDataRetriever.getTestData("client.bcc.user.name").toString(), TestDataRetriever.getTestData("client.bcc.user.pwd").toString());
       Assert.assertTrue(confirmIdentityModal.isIdentityModalDisplayed());

       IdentityConfirmedModal identityConfirmedModal = confirmIdentityModal.handleConfirmIdentityModal();
       Assert.assertNotNull(identityConfirmedModal);
       Assert.assertTrue(identityConfirmedModal.isIdentityConfirmedModalDisplayed());

       DriverCommand.takeScreenshot("Identity Confirmed - Continue to Online Banking - Test Evidence");
       HomePage homePage = identityConfirmedModal.handleIdentityConfirmed();
       //Assert.assertTrue(homePage.isHomePageDisplayed());

       DriverCommand.takeScreenshot("Logged-In User's HomePage is displayed");

       homePage.isDownloadTrusteerPopUpDisplayed();

       //Check BCC Offer
       cardAdminHomePage = new CardAdminHomePage();
       cardAdminHomePage.bccOfferDisplayedScreenshot();
       Assert.assertEquals(cardAdminHomePage.checkBccOfferDisplay().get(0),"RECOMMENDATIONS");
       Assert.assertEquals(cardAdminHomePage.checkBccOfferDisplay().get(1),"A Card Designed for Growing Companies");
       Assert.assertEquals(cardAdminHomePage.checkBccOfferDisplay().get(2),"You're pre-qualified for an SVB Innovators Card Program.");
       Assert.assertEquals(cardAdminHomePage.checkBccOfferDisplay().get(3),"LEARN MORE");

       //Logout the current user session
       cardAdminHomePage.logoutSession();
       DriverCommand.takeScreenshot("Logout the user session successfully", currentTestCaseName);
       }

}
