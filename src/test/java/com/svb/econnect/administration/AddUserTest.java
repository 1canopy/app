package com.svb.econnect.administration;

import com.svb.annotations.NeedBrowser;
import com.svb.constants.BankUserGroupType;
import com.svb.constants.TestGroupTags;
import com.svb.core.EnvironmentUtils;
import com.svb.core.TestDataRetriever;
import com.svb.drivers.BasePageSetup;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import com.svb.pages.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by dikuppan on 03/02/2018
 */
public class AddUserTest extends BasePageSetup {

    private static Logger logger = LogManager.getLogger(AddUserTest.class);

    private RemoteWebDriver driver;
    private LoginPage loginPage;
    private HomePage homePage;
    private UserAdministrationPage userAdministrationPage;
    private ManageUserAdministration manageUserAdministration;

    private String currentTestCaseName;

    @DataProvider(name = "BankUserGroupsNewUser")
    public static Object[][] bankGroupsUserType() {
        Object[][] objectData = new Object[BankUserGroupType.values().length][1];
        int counter = 0;
        for (BankUserGroupType bankUserTypeName : BankUserGroupType.values()) {
            objectData[counter][0] = bankUserTypeName.getUserRoleType();
            counter++;
        }
        return objectData;
    }

    @NeedBrowser
    @Test(groups = {TestGroupTags.SMOKE}, dataProvider = "BankUserGroupsNewUser")
    public void addUserTest(final ITestContext testContext, String bankUserType) {

        currentTestCaseName = testContext.getName();
        DriverCommand.setThreadTestName(currentTestCaseName);

        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(driver, this);

        driver.get(EnvironmentUtils.getUrl());
        driver.manage().window().maximize();

        LoginPage loginPage = new LoginPage();
        ConfirmIdentityModal confirmIdentityModal = loginPage.login(TestDataRetriever.getTestData("bank.user.name").toString(), TestDataRetriever.getTestData("bank.user.pwd").toString());
        Assert.assertTrue(confirmIdentityModal.isIdentityModalDisplayed());

        IdentityConfirmedModal identityConfirmedModal = confirmIdentityModal.handleConfirmIdentityModal();
        Assert.assertNotNull(identityConfirmedModal);
        Assert.assertTrue(identityConfirmedModal.isIdentityConfirmedModalDisplayed());

        DriverCommand.takeScreenshot("Identity Confirmed - Continue to Online Banking - Test Evidence");
        homePage = identityConfirmedModal.handleIdentityConfirmed();
        //Assert.assertTrue(homePage.isHomePageDisplayed());
        DriverCommand.takeScreenshot("Logged-In User's HomePage is displayed");
        homePage.isDownloadTrusteerPopUpDisplayed();

        homePage.clickMenuAction("Administration", "User Administration");
        manageUserAdministration = homePage.userAdmin();
        manageUserAdministration.selectUserGroup(bankUserType);
        manageUserAdministration.addNewUser();
        manageUserAdministration.fillUserDetails(bankUserType);
        DriverCommand.takeScreenshot("Assign Services page for the user");

        manageUserAdministration.selectAllServicesAndSubmit();

        manageUserAdministration.verifyServicesAssignedAndApprove();

        Assert.assertEquals("Set-up Confirmed", manageUserAdministration.setUpConfirmed(), "Verify the Set-up Confirmed for new/modify user");

        //Logout the current user session
        homePage.logoutSession();
        DriverCommand.takeScreenshot("Logout the user session successfully", currentTestCaseName);
    }
}
