package com.svb.econnect.payments;

import com.svb.annotations.NeedBrowser;
import com.svb.constants.TestGroupTags;
import com.svb.core.EnvironmentUtils;
import com.svb.core.TestDataRetriever;
import com.svb.drivers.BasePageSetup;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import com.svb.pages.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.Test;

/**
 * Created by dikuppan on 03/06/2018
 */
public class MakeTransferTest extends BasePageSetup {

    private RemoteWebDriver driver;
    private HomePage homePage;
    private TransferPage transferPage;
    private String currentTestCaseName;
    private LoginPage loginPage;

    private static Logger logger = LogManager.getLogger(MakeTransferTest.class);

    /*@BeforeTest(groups = {"smoke"})
    public void initiateAppSetUp(final ITestContext testContext) {
        *//*driver = getDriver();
        CustomPageFactory.initElements(driver, this);*//*
        currentTestCaseName = testContext.getName();
        System.out.println("Current Executing Test Case Name : " + currentTestCaseName);
        DriverManager.setCurrentTestCaseName(currentTestCaseName);
    }*/

    @NeedBrowser
    @Test(groups = {TestGroupTags.SMOKE})
    public void makeOneTimeTransferTest(final ITestContext testContext) {
        currentTestCaseName = testContext.getName();
        DriverCommand.setThreadTestName(currentTestCaseName);

        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(driver, this);

        driver.get(EnvironmentUtils.getUrl());
        //driver.manage().window().maximize();

        loginPage = new LoginPage();
        ConfirmIdentityModal confirmIdentityModal = loginPage.login(TestDataRetriever.getTestData("client.user.name").toString(), TestDataRetriever.getTestData("client.user.pwd").toString());
        Assert.assertTrue(confirmIdentityModal.isIdentityModalDisplayed());

        IdentityConfirmedModal identityConfirmedModal = confirmIdentityModal.handleConfirmIdentityModal();
        Assert.assertNotNull(identityConfirmedModal);
        Assert.assertTrue(identityConfirmedModal.isIdentityConfirmedModalDisplayed());

        DriverCommand.takeScreenshot("Identity Confirmed - Continue to Online Banking - Test Evidence", currentTestCaseName);
        homePage = identityConfirmedModal.handleIdentityConfirmed();
        //Assert.assertTrue(homePage.isHomePageDisplayed());

        DriverCommand.takeScreenshot("Logged-In User's HomePage is displayed", currentTestCaseName);

        homePage.isDownloadTrusteerPopUpDisplayed();
        homePage.clickMenuAction("Payments & Transfers", "Make a Transfer");
        transferPage = homePage.navigateToTransferLink();
        Assert.assertTrue(transferPage.isTranferPageDisplayed());
        DriverCommand.takeScreenshot("Navigated to \"Make a Transfer\" Page is displayed - Test Evidence", currentTestCaseName);
        transferPage.makeNewTransfer(TestDataRetriever.getTestData("client.user.account.num.transfer.from").toString(), TestDataRetriever.getTestData("client.user.account.num.transfer.to").toString(), "12.00", "For Testing Use", "One-time", transferPage.getNextBusinessDay());
        transferPage.performTransfer("Confirm Transfer");
        transferPage.transferConfirmPage();
        Reporter.log("Bank Reference ID # : " + transferPage.getBankReferenceId());
        homePage.logoutSession();
        DriverCommand.takeScreenshot("Logout the user session successfully", currentTestCaseName);
    }
}
