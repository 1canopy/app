package com.svb.econnect.assignPermissions;

import com.svb.annotations.NeedBrowser;
import com.svb.constants.ServiceType;
import com.svb.constants.TestGroupTags;
import com.svb.core.EnvironmentUtils;
import com.svb.core.TestDataRetriever;
import com.svb.drivers.BasePageSetup;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import com.svb.pages.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.Test;

public class ModifyClientPermissionsTest {

    private static Logger logger = LogManager.getLogger(ModifyClientPermissionsTest.class);

    private RemoteWebDriver driver;
    private LoginPage loginPage;
    private HomePage homePage;
    private ModifyClientServicesPage modifyClientServicesPage;

    private String currentTestCaseName;

    @NeedBrowser
    @Test(groups = {TestGroupTags.SMOKE})
    public void modifyClientPermissionsBankUser(final ITestContext testContext) {

        currentTestCaseName = testContext.getName();
        DriverCommand.setThreadTestName(currentTestCaseName);

        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(driver, this);

        driver.get(EnvironmentUtils.getUrl());

        loginPage = new LoginPage();
        ConfirmIdentityModal confirmIdentityModal = loginPage.login(TestDataRetriever.getTestData("bank.user.name").toString(), TestDataRetriever.getTestData("bank.user.pwd").toString());
        Assert.assertTrue(confirmIdentityModal.isIdentityModalDisplayed());

        IdentityConfirmedModal identityConfirmedModal = confirmIdentityModal.handleConfirmIdentityModal();
        Assert.assertNotNull(identityConfirmedModal);
        Assert.assertTrue(identityConfirmedModal.isIdentityConfirmedModalDisplayed());

        DriverCommand.takeScreenshot("Identity Confirmed - Continue to Online Banking - Test Evidence");
        homePage = identityConfirmedModal.handleIdentityConfirmed();
        //Assert.assertTrue(homePage.isHomePageDisplayed());

        //homePage.isDownloadTrusteerPopUpDisplayed();
        DriverCommand.takeScreenshot("Logged-In User's HomePage is displayed");
        // homePage.selectAndSwitchClientLink();
        homePage.selectAndSwitchClient("Client Name", TestDataRetriever.getTestData("search.clientname").toString());

        ClientAdministrationPage clientAdministrationPage = homePage.navigateToClientAdministrationLink();
        Assert.assertTrue(clientAdministrationPage.isClientAdministrationPageDisplayed());

        clientAdministrationPage.navigateToClientServices();
        modifyClientServicesPage = new ModifyClientServicesPage();
        modifyClientServicesPage.enrollServices(ServiceType.UK_SERVICES.getServicesType());

        Assert.assertEquals("Client Administration : Confirmation", clientAdministrationPage.getClientConfirmationPageText().trim().replaceAll("\\r|\\n", ""));

        //Logout the current user session
        homePage.logoutSession();
        DriverCommand.takeScreenshot("Logout the bank user session successfully", currentTestCaseName);
    }

    @NeedBrowser
    @Test(groups = {TestGroupTags.SMOKE})
    public void verifyClientUser(final ITestContext testContext) {

        currentTestCaseName = testContext.getName();
        DriverCommand.setThreadTestName(currentTestCaseName);

        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(driver, this);

        driver.get(EnvironmentUtils.getUrl());

        loginPage = new LoginPage();
        ConfirmIdentityModal confirmIdentityModal = loginPage.login(TestDataRetriever.getTestData("client.uk.user.name").toString(), TestDataRetriever.getTestData("client.uk.user.pwd").toString());
        Assert.assertTrue(confirmIdentityModal.isIdentityModalDisplayed());

        IdentityConfirmedModal identityConfirmedModal = confirmIdentityModal.handleConfirmIdentityModal();
        Assert.assertNotNull(identityConfirmedModal);
        Assert.assertTrue(identityConfirmedModal.isIdentityConfirmedModalDisplayed());

        DriverCommand.takeScreenshot("Identity Confirmed - Continue to Online Banking - Test Evidence");
        homePage = identityConfirmedModal.handleIdentityConfirmed();
        //Assert.assertTrue(homePage.isHomePageDisplayed());

        homePage.isDownloadTrusteerPopUpDisplayed();
        DriverCommand.takeScreenshot("Logged-In User's HomePage is displayed");

        DriverCommand.takeScreenshot("UK Services HyperLink Available for the user - Test Evidence");
        Assert.assertTrue(homePage.isUKServicesEnabledUser());

        homePage.logoutSession();
        DriverCommand.takeScreenshot("Logout the client user session successfully", currentTestCaseName);
    }
}
