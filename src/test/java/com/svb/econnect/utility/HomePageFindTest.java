package com.svb.econnect.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HomePageFindTest {

    final WebDriver driver;
    @FindBy(how = How.NAME, using = "text")
    private WebElement helloText;
    @FindBy(how = How.NAME, using = "exit")
    private WebElement exitButton;

    public HomePageFindTest(WebDriver driver) {
        this.driver = driver;
    }

    public void clickExitButton() {
        exitButton.click();
    }

    public LoginPageFactoryTest logout() {
        clickExitButton();
        return PageFactory.initElements(driver, LoginPageFactoryTest.class);
    }
}