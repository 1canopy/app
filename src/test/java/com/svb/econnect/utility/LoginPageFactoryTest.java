package com.svb.econnect.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPageFactoryTest {

    final WebDriver driver;
    @FindBy(how = How.NAME, using = "login")
    private WebElement loginEdit;

    @FindBy(how = How.NAME, using = "pass")
    private WebElement passwordEdit;

    @FindBy(how = How.NAME, using = "button")
    private WebElement loginButton;

    public LoginPageFactoryTest(WebDriver driver) {
        this.driver = driver;
    }

    public void enterLogin(String login) {
        loginEdit.clear();
        loginEdit.sendKeys(login);
    }

    public void enterPassword(String password) {
        loginEdit.clear();
        passwordEdit.sendKeys(password);
    }

    public void clickLoginButton() {
        loginButton.click();
    }

    public HomePageFindTest login(String login, String password) {
        enterLogin(login);
        enterPassword(password);
        clickLoginButton();
        return PageFactory.initElements(driver, HomePageFindTest.class);
    }

}