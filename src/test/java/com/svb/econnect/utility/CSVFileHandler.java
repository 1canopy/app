package com.svb.econnect.utility;

import au.com.bytecode.opencsv.CSVParser;
import au.com.bytecode.opencsv.CSVReader;
import com.google.common.io.Resources;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

/**
 * Created by dikuppan on 03/07/2018
 */
public class CSVFileHandler {

    private static final Logger log = LoggerFactory.getLogger(CSVFileHandler.class);
    private static final String DEFAULT_CSV_EXECUTION_TAG = "groups.tag";
    private String filePath = "";

    /**
     * Default Constructor
     */
    public CSVFileHandler() {

    }

    public CSVFileHandler(String filePath) {
        log.info("CSVFileHandler Constructor invoked!!!");
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public static List<Object[]> fetchObjets(List<Map<String, String>> mapListRecords, String strExecuteBy) {

        LinkedList<Object[]> listObjectData = new LinkedList<>();

        if (StringUtils.isNotEmpty(strExecuteBy)) {
            for (int iMapListRecord = 0; iMapListRecord < mapListRecords.size(); iMapListRecord++) {
                boolean isExecuteByTestCaseFound = false;
                Map<String, String> stringStringMap = mapListRecords.get(iMapListRecord);
                LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>();
                for (Map.Entry<String, String> entry : stringStringMap.entrySet()) {

                    if (!entry.getValue().toLowerCase().contains(strExecuteBy.toLowerCase().trim()) && entry.getKey().toLowerCase().contains(DEFAULT_CSV_EXECUTION_TAG)) {
                        isExecuteByTestCaseFound = true;
                    } else if (entry.getValue().toLowerCase().contains(strExecuteBy.toLowerCase().trim())) {
                        isExecuteByTestCaseFound = true;
                    }
                    if (!entry.getValue().toLowerCase().equalsIgnoreCase("ignore")) {
                        linkedHashMap.put(entry.getKey().trim(), entry.getValue());
                    }
                    if (!entry.getValue().toLowerCase().equalsIgnoreCase("null") || entry.getValue().toLowerCase().equalsIgnoreCase("nullable")) {
                        log.info("Checking the value now " + entry.getValue().trim());
                        linkedHashMap.put(entry.getKey().trim(), "nullable");
                    }
                    if (!entry.getValue().toLowerCase().equalsIgnoreCase("removetag")) {
                        linkedHashMap.put(entry.getKey().trim(), "removetag");
                    }

                }
                if (!isExecuteByTestCaseFound) {
                    linkedHashMap.clear();
                }
                Object[] rowData = null;
                if (linkedHashMap.size() > 0) {
                    rowData = new Object[]{linkedHashMap};
                    listObjectData.add(rowData);
                }
            }
        }
        return listObjectData;
    }

    public static List<Object[]> fetchObjets(List<Map<String, String>> mapListRecords) {

        LinkedList<Object[]> listObjectData = new LinkedList<>();
        for (int iMapListRecord = 0; iMapListRecord < mapListRecords.size(); iMapListRecord++) {
            Map<String, String> stringStringMap = mapListRecords.get(iMapListRecord);
            LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>();
            for (Map.Entry<String, String> entry : stringStringMap.entrySet()) {
                if (!entry.getValue().toLowerCase().equalsIgnoreCase("ignore")) {
                    linkedHashMap.put(entry.getKey().trim(), entry.getValue());
                }
                if (!entry.getValue().toLowerCase().equalsIgnoreCase("null") || entry.getValue().toLowerCase().equalsIgnoreCase("nullable")) {
                    log.info("Checking the value now " + entry.getValue().trim());
                    linkedHashMap.put(entry.getKey().trim(), "nullable");
                }
                if (!entry.getValue().toLowerCase().equalsIgnoreCase("removetag")) {
                    linkedHashMap.put(entry.getKey().trim(), "removetag");
                }

            }
            Object[] rowData = null;
            if (linkedHashMap.size() > 0) {
                rowData = new Object[]{linkedHashMap};
                listObjectData.add(rowData);
            }
        }
        return listObjectData;
    }

    public static Object[][] transform(List<Object[]> listObjectData) {
        try {
            Object[][] objectData = new Object[listObjectData.size()][];
            objectData = listObjectData.toArray(objectData);
            return objectData;
        } catch (NullPointerException nullPointerException) {
            throw new NullPointerException("NullPointerException received for the arrayList object");
        }
    }

    /**
     * Get the absolute file path
     *
     * @return
     */
    private String getAbsoluteFilePath() throws Exception {
        String resourcePath = null;
        try {
            URL urlPath = Resources.getResource(getFilePath());
            resourcePath = urlPath.toURI().getPath();
        } catch (URISyntaxException uriSyntaxException) {
            throw new Exception("File Not Found in the specified resources or path", uriSyntaxException);
        }
        return resourcePath;
    }

    /**
     * Fetch records from CSV file
     * <p>
     * Usage : Read CSV file & Stores as List-Map object
     *
     * @return
     * @throws Exception
     */
    public List<Map<String, String>> fetchRecords() throws Exception {
        String absFilePath = getAbsoluteFilePath();

        List<Map<String, String>> mapListRecords = new ArrayList<>();

        try (FileReader fileReader = new FileReader((absFilePath))) {
            CSVReader reader = new CSVReader(fileReader, CSVParser.DEFAULT_QUOTE_CHARACTER);

            List<String> headers = Arrays.asList(reader.readNext());

            String[] nextLineRecoreds;
            int iRecordCount = 0;
            while ((nextLineRecoreds = reader.readNext()) != null) {
                Map<String, String> subHeaderRecordsIndex = new HashMap();
                for (int iHeaders = 0; iHeaders < headers.size(); iHeaders++) {
                    if (nextLineRecoreds[iHeaders] != null) {
                        subHeaderRecordsIndex.put(headers.get(iHeaders).trim(), nextLineRecoreds[iHeaders]);
                    }
                }
            }

        } catch (IOException ioException) {
            System.out.println("Problem reading CSV" + ioException);
        }
        return mapListRecords;
    }
}
