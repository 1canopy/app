package com.svb.econnect.audit;

import com.svb.annotations.NeedBrowser;
import com.svb.constants.TestGroupTags;
import com.svb.core.EnvironmentUtils;
import com.svb.core.TestDataRetriever;
import com.svb.drivers.BasePageSetup;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import com.svb.pages.ConfirmIdentityModal;
import com.svb.pages.HomePage;
import com.svb.pages.IdentityConfirmedModal;
import com.svb.pages.LoginPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.Test;

/**
 * Created by dikuppan on 02/27/2018
 */
public class UserActivityAllTransfersPageTest extends BasePageSetup {

    private static Logger logger = LogManager.getLogger(UserActivityAllTransfersPageTest.class);

    private RemoteWebDriver driver;
    private LoginPage loginPage;
    private String currentTestCaseName;

    @NeedBrowser
    @Test(groups = {TestGroupTags.SMOKE})
    public void userActivityAllTransfersPage(final ITestContext testContext) {

        currentTestCaseName = testContext.getName();
        DriverCommand.setThreadTestName(currentTestCaseName);

        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(driver, this);

        driver.get(EnvironmentUtils.getUrl());
        //driver.manage().window().maximize();

        loginPage = new LoginPage();

        ConfirmIdentityModal confirmIdentityModal = loginPage.login(TestDataRetriever.getTestData("client.user.name").toString(), TestDataRetriever.getTestData("client.user.pwd").toString());
        Assert.assertTrue(confirmIdentityModal.isIdentityModalDisplayed());

        IdentityConfirmedModal identityConfirmedModal = confirmIdentityModal.handleConfirmIdentityModal();
        Assert.assertNotNull(identityConfirmedModal);
        Assert.assertTrue(identityConfirmedModal.isIdentityConfirmedModalDisplayed());

        DriverCommand.takeScreenshot("Identity Confirmed - Continue to Online Banking - Test Evidence");
        HomePage homePage = identityConfirmedModal.handleIdentityConfirmed();
        Assert.assertTrue(homePage.isHomePageDisplayed());
    }

}