package com.svb.pages;

import com.svb.annotations.SVBFindBy;
import com.svb.annotations.Until;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.openqa.selenium.WebElement;

public class ClientAdministrationPage {

    @SVBFindBy(xpath = "//a[contains(text(),\"Maintain Client Authenticate\")]")
    private WebElement linkClientAuthentication;


    @SVBFindBy(xpath = "//a[contains(text(),\"Modify Client Services/Billing Information\")]")
    private WebElement linkModifyClientServices;

    @SVBFindBy(until = Until.VISIBLE, timeout = 5, xpath = "//span[@class='pageTitle']")
    private WebElement headingLabel;

    @SVBFindBy(until = Until.VISIBLE, timeout = 5, xpath = "//td[@class='pageTitle']")
    private WebElement clientConfirmationPageTitle;

    public ClientAdministrationPage() {
        CustomPageFactory.initElements(DriverManager.getDriver(), this);
    }

    public MaintainClientAuthenticatePage navigateToClientAuthentication() {
        DriverCommand.takeScreenshot("Navigated to \"Client Administration\" Page - Test Evidence");
        linkClientAuthentication.click();
        return new MaintainClientAuthenticatePage();
    }

    public MaintainClientAuthenticatePage navigateToClientServices() {
        DriverCommand.takeScreenshot("Navigated to \"Client Administration\" Page - Test Evidence");
        linkModifyClientServices.click();
        return new MaintainClientAuthenticatePage();
    }

    public String getHeadingText() {
        return headingLabel.getText();
    }

    public String getClientConfirmationPageText() {
        return clientConfirmationPageTitle.getText();
    }

    public boolean isClientAdministrationPageDisplayed() {
        return headingLabel.isDisplayed();
    }
}
