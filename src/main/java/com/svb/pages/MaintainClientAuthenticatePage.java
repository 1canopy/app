package com.svb.pages;

import com.svb.annotations.SVBFindBy;
import com.svb.constants.ClientAuthStatus;
import com.svb.core.TestDataRetriever;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;

public class MaintainClientAuthenticatePage {

    private static Logger logger = LogManager.getLogger(MaintainClientAuthenticatePage.class);

    @SVBFindBy(xpath = "//td[@class='pageTitle' and contains(text(),'Maintain Client Authenticate')]")
    private WebElement currentPageTitle;

    @SVBFindBy(xpath = "//input[@name='clientCTVStatus' and @type='radio' and @value='Enabled']")
    @CacheLookup
    private WebElement enableClientAuthenticate;

    @SVBFindBy(xpath = "//input[@name='clientCTVStatus' and @type='radio' and @value='Disabled']")
    @CacheLookup
    private WebElement disableClientAuthenticate;

    @SVBFindBy(name = "Save")
    private WebElement saveButton;

    public MaintainClientAuthenticatePage() {
        CustomPageFactory.initElements(DriverManager.getDriver(), this);
    }

    public WebElement getCurrentPageTitle() {
        return currentPageTitle;
    }

    /**
     * @param actionName - (Enable/Disable)
     * @return
     */
    public ClientAdministrationPage manageClientAuthentication(String actionName) {
        DriverCommand.takeScreenshot("Before " + actionName + " the \"authenticate\" status for the client - \"" + TestDataRetriever.getTestData("search.clientname").toString() + "\" - Test Evidence");
        if (actionName.trim().equalsIgnoreCase(ClientAuthStatus.ENABLE.getStatus())) {
            enableClientAuthenticate.click();
        } else if (actionName.trim().equalsIgnoreCase(ClientAuthStatus.DISABLE.getStatus())) {
            disableClientAuthenticate.click();
        }
        DriverCommand.takeScreenshot("After \"authenticate\" status " + actionName + " for the client - \"" + TestDataRetriever.getTestData("search.clientname").toString() + "\" - Test Evidence");
        saveButton.click();
        return new ClientAdministrationPage();
    }

    public boolean isClientAuthenticationPageDisplayed() {
        return currentPageTitle.isDisplayed();
    }

    /**
     * Should return - Enable/Disable
     *
     * @return
     */
    public String verifyClientAuthCheckedStatus() {
        String authStatus = (isClientAuthenticationEnabled() ? ClientAuthStatus.ENABLE.getStatus() : ClientAuthStatus.DISABLE.getStatus());
        DriverCommand.takeScreenshot("Client Authentication Status - \"" + TestDataRetriever.getTestData("search.clientname").toString() + "\" - \"" + authStatus + "\" - Test Evidence");
        return authStatus;
    }

    /**
     * Client Authentication status should return - True/False
     *
     * @return
     */
    public boolean isClientAuthenticationEnabled() {
        return enableClientAuthenticate.isSelected() ? true : false;
    }
}