package com.svb.pages;

import com.svb.annotations.SVBFindBy;
import com.svb.constants.TransferType;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class TransferPage {

    @SVBFindBy(xpath = "//button[@id='addUser']")
    private WebElement addUser;

    @SVBFindBy(linkText = "Cancel")
    private WebElement cancelButton;

    @SVBFindBy(xpath = "//h1[contains(text(),'Make a Transfer') and @class='svb-page-header-heading']")
    private WebElement currentPageHeading;

    @SVBFindBy(name = "transferAmountStr")
    private WebElement transferAmountField;

    @SVBFindBy(name = "transferDescription")
    private WebElement transferDescriptionField;

    @SVBFindBy(name = "transferDateStr")
    private WebElement transferDateField;

    @SVBFindBy(xpath = "//button[@id=\"nxtBtton\" and contains(text(),\"Next\")]")
    private WebElement makeTransferButton;

    @SVBFindBy(xpath = "//input[@name=\"schedule\" and @type=\"radio\" and @value=\"onetime\"]")
    private WebElement selectOneTimeTransferScheduleType;

    @SVBFindBy(xpath = "//input[@name=\"schedule\" and @type=\"radio\" and @value=\"recurring\"]")
    private WebElement selectRecurringTransferScheduleType;

    @SVBFindBy(xpath = "//h2[@class=\"svb-page-header-subheading\" and contains(text(),\"Verify your details\")]")
    private WebElement verifyTransactionPageTitle;

    @SVBFindBy(xpath = "//button[@data-bind=\"click : confirm, enable : !processing(), css : { 'svb-disabled' : processing() }\"]")
    private WebElement confirmTransferBtn;

    @SVBFindBy(xpath = "//button[@data-bind=\"click : cancel\"]")
    private WebElement cancelTransferBtn;

    @SVBFindBy(xpath = "//button[@data-bind=\"click : edit\"]")
    private WebElement editTransferBtn;

    @SVBFindBy(xpath = "//h2[@class=\"svb-page-header-subheading\" and contains(text(),\"Confirmation\")]")
    private WebElement transConfirmationPageTitle;

    @SVBFindBy(xpath = "//dd[@class=\"svb-form-data\" and @data-bind=\"text : data.transfer.id\"]")
    private WebElement bankRefID;

    WebDriver driver;
    CommonUtilities commonUtilities;

    public TransferPage() {
        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(driver, this);
        commonUtilities = new CommonUtilities();
    }

/*    public MaintainClientAuthenticatePage navigateToClientAuthentication() {
        DriverCommand.takeScreenshot("Navigated to \"User Administration\" Page - Test Evidence");
        linkClientAuthentication.click();
        DriverCommand.takeScreenshot("Navigated to \"Maintain Client Authenticate\" link clicked - Test Evidence");
        return new MaintainClientAuthenticatePage();
    }*/

    public String getHeadingText() {
        return currentPageHeading.getText();
    }

    public boolean isTranferPageDisplayed() {
        return currentPageHeading.isDisplayed();
    }

    public void initiateTransferFromAccount(String selectFromAccountValue) {
        Select selectFromAccountDetail = new Select(driver.findElement(By.name("fromAccountId")));//id - By.id("transferfromfield");
        selectFromAccountDetail.selectByVisibleText(selectFromAccountValue);
    }

    public void initiateTransferToAccount(String selectToAccountValue) {
        Select selectFromAccountDetail = new Select(driver.findElement(By.name("toAccountId")));//id - By.id("transfertofield");
        selectFromAccountDetail.selectByVisibleText(selectToAccountValue);
    }

    public void initiateTransferFromAccountByValue(String selectFromAccountValue) {
        Select selectFromAccountDetail = new Select(driver.findElement(By.name("fromAccountId")));//id - By.id("transferfromfield");
        selectFromAccountDetail.selectByValue(selectFromAccountValue);
    }

    public void initiateTransferToAccountByValue(String selectToAccountValue) {
        Select selectFromAccountDetail = new Select(driver.findElement(By.name("toAccountId")));//id - By.id("transfertofield");
        selectFromAccountDetail.selectByValue(selectToAccountValue);
    }

    /**
     * This method initiate the transfer with the all the below filled
     *
     * @param fromAccount
     * @param toAccount
     * @param transferAmount
     * @param transferDesc
     * @param scheduleType
     * @param transferDate
     */
    public void makeNewTransfer(String fromAccount, String toAccount, String transferAmount, String transferDesc, String scheduleType, String transferDate) {

        initiateTransferFromAccountByValue(fromAccount);
        initiateTransferToAccountByValue(toAccount);
        transferAmountField.sendKeys(transferAmount);
        if (null != transferDesc) {
            transferDescriptionField.sendKeys(transferDesc);
        }
        if (scheduleType.trim().equalsIgnoreCase(TransferType.ONETIME.getTransferType())) {
            if (!selectOneTimeTransferScheduleType.isSelected()) {
                selectOneTimeTransferScheduleType.click();
            }
        } else if (scheduleType.trim().equalsIgnoreCase(TransferType.RECURRING.getTransferType())) {
            selectRecurringTransferScheduleType.click();
        }
        if (null != transferDate) {
            transferDateField.clear();
            transferDateField.sendKeys(transferDate);
        }
        DriverCommand.takeScreenshot("Make a Transfer Page Filled with values From, To, Amount, Date & Transfer Type - Test Evidence");
        makeTransferButton.click();
    }

    /**
     * @param fromAccount
     * @param toAccount
     * @param transferAmount
     * @param scheduleType
     * @param transferDate
     */
    public void makeNewTransfer(String fromAccount, String toAccount, String transferAmount, String scheduleType, String transferDate) {
        makeNewTransfer(fromAccount, toAccount, transferAmount, null, scheduleType, transferDate);
    }

    /**
     * @param actionName
     */
    public void performTransfer(String actionName) {
        commonUtilities.waitForElementPresent(this.driver, By.xpath("//h2[@class=\"svb-page-header-subheading\" and contains(text(),\"Verify your details\")]"));
        DriverCommand.takeScreenshot("Before confirming the transfer in \"Verify your details\" Page for the current transaction request - Test Evidence");
        if (actionName.trim().equalsIgnoreCase("Confirm Transfer")) {
            confirmTransferBtn.click();
        } else if (actionName.trim().equalsIgnoreCase("Cancel")) {
            cancelTransferBtn.click();
        } else if (actionName.trim().equalsIgnoreCase("Edit")) {
            editTransferBtn.click();
        } else {
            throw new IllegalArgumentException("Invalid action details performed - Action Name : " + actionName);
        }
    }

    public void transferConfirmPage() {
        commonUtilities.waitForElementPresent(this.driver, By.xpath("//h2[@class=\"svb-page-header-subheading\" and contains(text(),\"Confirmation\")]"));
        DriverCommand.takeScreenshot("After the transfer got initiated successfully in Confirmation Page");
        Assert.assertEquals("Confirmation", transConfirmationPageTitle.getText().trim(), "Transfer initiate confirmed");
    }

    /**
     * Currently getting the business day which is valid date for MON-FRI
     *
     * @return
     */
    public String getNextBusinessDay() {
        DateTime datetime = new DateTime();
        datetime = datetime.plusDays(1);
        if (datetime.getDayOfWeek() == DateTimeConstants.SUNDAY) {
            datetime = datetime.plusDays(1);
        } else if (datetime.getDayOfWeek() == DateTimeConstants.SATURDAY) {
            datetime = datetime.plusDays(2);
        }
        return (datetime.getMonthOfYear() + "/" + datetime.getDayOfMonth() + "/" + datetime.getYear());
    }

    public String getBankReferenceId() {
        return bankRefID.getText();
    }
}
