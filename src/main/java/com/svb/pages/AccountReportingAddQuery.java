package com.svb.pages;

import com.github.javafaker.Faker;
import com.svb.annotations.SVBFindBy;
import com.svb.annotations.Until;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.regex.Pattern;

/**
 * Class : Account Reporting - Create New Query Page base page
 */
public class AccountReportingAddQuery {

    private static Logger logger = LogManager.getLogger(AccountReportingAddQuery.class);
    private static final Faker fakeTitleGenerator = new Faker();

    @SVBFindBy(xpath = "//td[@class='pageTitle' and contains(text(),'Create Query')]")
    private WebElement currentPageTitle;

    @SVBFindBy(name = "Go")
    private WebElement goButton;

    @SVBFindBy(until = Until.VISIBLE, timeout = 5, xpath = "//select[@name='statementPeriod']")
    private WebElement statementPeriod;

    @SVBFindBy(until = Until.VISIBLE, timeout = 5, xpath = "//select[@name='selectedStatementType']")
    private WebElement selectedStatementType;

    @SVBFindBy(name = "queryName")
    private WebElement statementQueryName;

    @SVBFindBy(name = "selectedShareQueryOption")
    private WebElement selectedShareQueryOption;

    @SVBFindBy(xpath = "//input[@name='run' and @value='Test' and @type='button']")
    private WebElement testRunQueryButton;

    @SVBFindBy(xpath = "//input[@value='Save' and @type='button']")
    private WebElement saveQueryButton;

    @SVBFindBy(xpath = "//input[@value='Cancel' and @type='button']")
    private WebElement cancelQueryButton;

    WebDriver driver;
    CommonUtilities commonUtilities;

    public AccountReportingAddQuery() {
        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(driver, this);
        commonUtilities = new CommonUtilities();
    }

    /**
     * @param reportingType
     */
    public void selectQueryCategoryType(String reportingType, String statementPeriodValue, String statementTypeValue) {
        String DEFAULT_ALPHA_REGEXP = "^[a-zA-Z0-9_\\-\\s]*$";
        String fakeNameValidator = "" + fakeTitleGenerator.name().fullName().replace(".", "");
        Pattern p = Pattern.compile(DEFAULT_ALPHA_REGEXP);
        String tempAddQueryName = "";
        if (p.matcher(fakeNameValidator).find()) {
            tempAddQueryName = fakeNameValidator;
            logger.info("No special characters in the Random Query Name" + tempAddQueryName);
        } else {
            tempAddQueryName = fakeNameValidator.replaceAll(DEFAULT_ALPHA_REGEXP, "");
            logger.info("Removed the special characters in the Random Query Name " + tempAddQueryName);
        }
        tempAddQueryName += "_" + statementTypeValue + "_AddQuery";
        Select queryCategoryTypeDropDown = commonUtilities.selectByDropDownValue(this.driver, By.xpath("//select[@name='selectedCategory']"));
        queryCategoryTypeDropDown.selectByVisibleText(reportingType);

        DriverCommand.takeScreenshot("Create Query - Test Evidence");
        goButton.click();

        Select statementPeriodDropDown = commonUtilities.selectByDropDownValue(this.driver, By.xpath("//select[@name='statementPeriod']"));
        statementPeriodDropDown.selectByVisibleText(statementPeriodValue);

        Select statementTypeDropDown = commonUtilities.selectByDropDownValue(this.driver, By.xpath("//select[@name='selectedStatementType']"));
        statementTypeDropDown.selectByVisibleText(statementTypeValue);

        statementQueryName.sendKeys(tempAddQueryName);
        DriverCommand.takeScreenshot("Auto-generated Create Query Name : \"" + tempAddQueryName + "\"- Test Evidence");
        saveQueryButton.click();

        DriverCommand.takeScreenshot("Query Created Successfully - Test Evidence");
    }

    public boolean isCreateQueryPageDisplayed() {
        return currentPageTitle.isDisplayed();
    }

}
