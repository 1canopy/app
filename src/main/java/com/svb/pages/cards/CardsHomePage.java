package com.svb.pages.cards;

import com.svb.annotations.SVBFindBy;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import com.svb.pages.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

public class CardsHomePage {

    private static Logger logger = LogManager.getLogger(CardsHomePage.class);

    //@SVBFindBy(id = "logoutBtn")
    @SVBFindBy(id = "logoutBtn")
    private WebElement logoutButton;

    @SVBFindBy(xpath = "//a[@href='/useraccess/Mainmenu.jsp']")
    private WebElement mainMenuHyperlink;

    @SVBFindBy(xpath = "//div[contains(@id,'close-button') and @title='Close']")
    private WebElement splashWindowClose;

    @SVBFindBy(id = "openClientDialog")
    private WebElement selectSwitchClientLink;

    @SVBFindBy(linkText = "Client Administration")
    private WebElement localLinkClientAdministration;

    @SVBFindBy(linkText = "User Administration")
    private WebElement linkUserAdministration;

    @SVBFindBy(xpath = "//a[@class='svb-uk-services-link']")
    private WebElement linkUKServices;

    WebDriver driver;
    CommonUtilities commonUtilities;

    public CardsHomePage() {
        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(driver, this);
        commonUtilities = new CommonUtilities();
    }

    /**
     * @param searchBy
     * @param clientSearchText
     */
    public void selectAndSwitchClient(String searchBy, String clientSearchText) {
        selectAndSwitchClientLink();
        DriverCommand.takeScreenshot("Select/Switch Client dialog window - Test Evidence");
        SwitchClientDialog switchClientDialog = new SwitchClientDialog();
        switchClientDialog.selectAndSwitchClient(searchBy, clientSearchText);
    }

    /**
     * @return
     */
    public ClientAdministrationPage navigateToClientAdministrationLink() {
        localLinkClientAdministration.click();
        return new ClientAdministrationPage();
    }

    /**
     * @return
     */
    public UserAdministrationPage navigateToUserAdministrationLink() {
        linkUserAdministration.click();
        return new UserAdministrationPage();
    }

    /**
     * @return
     */
    public TransferPage navigateToTransferLink() {
        return new TransferPage();
    }

    /**
     * @return
     */
    public ManageUserAdministration userAdmin() {
        return new ManageUserAdministration();
    }

    public StatementsPage clickMainMenuHyperLink() {
        mainMenuHyperlink.click();
        DriverCommand.takeScreenshot("Main Menu Page - Test Evidence");
        return new StatementsPage();
    }

    public boolean isHomePageDisplayed() {
        return logoutButton.isDisplayed();
    }

    public void selectAndSwitchClientLink() {
        isDownloadTrusteerPopUpDisplayed();
        selectSwitchClientLink.click();
    }

    public void isDownloadTrusteerPopUpDisplayed() {
        DriverCommand.takeScreenshot("\"Download Trusteer\" Popup Displayed - Test Evidence");
        try {
            if (CommonUtilities.isElementPresent(splashWindowClose)) {
                splashWindowClose.click();
            } else {
                Reporter.log("\"Splash Trusteer Plugin Download\" pop not displayed this time on a page - ");// Using the TestNG API for logging
            }
        } catch (NoSuchElementException noException) {
            Reporter.log("No Download Trusteer Popup Displayed");
            Reporter.log("Element: " + ", is not available on a page - ");// Using the TestNG API for logging
        }
        DriverCommand.takeScreenshot("After Closing the \"Download Trusteer\" Popup Displayed Back to Home Page- Test Evidence");
    }

    public void clickMenuAction(String parentMenu, String childMenu) {
        commonUtilities.onMouseHoverAction(this.driver, By.linkText(parentMenu), By.linkText(childMenu));
    }

    public AccountReportingAddQuery accountReportingAddQueryPage() {
        DriverCommand.takeScreenshot("Account - Create Query Page Page - Test Evidence");
        return new AccountReportingAddQuery();
    }

    public boolean isElementPresent(String linkText) throws NoSuchElementException {
        WebElement webElement = this.driver.findElement(By.linkText(linkText));
        return webElement.isDisplayed();
    }

    public boolean isUKServicesEnabledUser(){
        return linkUKServices.isDisplayed();
    }

    public void cardHomePage(String cardUserType)
    {
    	switch(cardUserType)
    	{
    	case "CardAdmin":
    		new CardAdminHomePage();
    		break;
    	case "CardUX":
    		new CardUXHomePage();
    		break;
    	case "CombinationUser":
    		new CardCombinationHomePage();
    		break;
    	default:
    		Reporter.log("Invalid card user. Provide correct card user type. (CardAdmin/CardUX/CombinationUser) ");
    	}
    }
    public void logoutSession() {
        logoutButton.click();
    }
}