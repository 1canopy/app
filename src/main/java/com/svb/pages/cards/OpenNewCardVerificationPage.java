package com.svb.pages.cards;

import com.svb.annotations.SVBFindBy;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OpenNewCardVerificationPage {
private static Logger logger = LogManager.getLogger(OpenNewCardVerificationPage.class);

	@SVBFindBy(xpath = "//label[@for='svb-checkbox']")
	private WebElement termsAgreedCheckBox;

	@SVBFindBy(xpath = "//button[@class=\"ca-button activeBlue confirmbtn\"]")
	private WebElement confirmBtn;



	CommonUtilities commonUtilities;
	WebDriver driver;

	public OpenNewCardVerificationPage() {
		driver = DriverManager.getDriver();
		CustomPageFactory.initElements(DriverManager.getDriver(), this);
		commonUtilities = new CommonUtilities();
	}

	public void submitVerification(){
		try {
			Thread.sleep(5 * 1000);
		} catch (Exception exception) {

		}
		termsAgreedCheckBox.click();
		confirmBtn.click();
	}
}

