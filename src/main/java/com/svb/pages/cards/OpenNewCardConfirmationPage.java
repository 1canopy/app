package com.svb.pages.cards;

import com.svb.annotations.SVBFindBy;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OpenNewCardConfirmationPage {
private static Logger logger = LogManager.getLogger(OpenNewCardConfirmationPage.class);

	@SVBFindBy(xpath = "//button[@class=\"ca-button activeBlue donebtn\"]")
	private WebElement DoneBtn;



	CommonUtilities commonUtilities;
	WebDriver driver;

	public OpenNewCardConfirmationPage() {
		driver = DriverManager.getDriver();
		CustomPageFactory.initElements(DriverManager.getDriver(), this);
		commonUtilities = new CommonUtilities();
	}

	public void verifyConfirmation(){
		try {
			Thread.sleep(2 * 1000);
		} catch (Exception exception) {

		}
		DoneBtn.click();
	}
}

