package com.svb.pages.cards;

import com.github.javafaker.Faker;
import com.svb.annotations.SVBFindBy;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.text.SimpleDateFormat;
import java.util.Date;

public class OpenNewCardPage {
    private static Logger logger = LogManager.getLogger(OpenNewCardPage.class);
    private static final Faker fakeTitleGenerator = new Faker();

    @SVBFindBy(xpath = "//div[@class='new-card-label' and text()='New Card Request']")
    private WebElement pageTitle;

    @SVBFindBy(xpath = "//label[@for='traditionalCard']")
    private WebElement plasticCardRadioBtn;

    @SVBFindBy(xpath = "//div[@class='label' and text()='Plastic']")
    private WebElement plasticCardRadioLabel;

    @SVBFindBy(xpath = "//label[@for='ghostCard']")
    private WebElement nonPlasticCardRadioBtn;

    @SVBFindBy(xpath = "//*[@id='Ghostcard0']/span[1]")
    private WebElement nonPlasticCardRadioLabel;

    @SVBFindBy(xpath = "//span[@class='label' and text()='Cardholder First Name']")
    private WebElement cardholderFirstnameTextBoxLabel;

    @SVBFindBy(xpath = "//input[@class='ca-input-text ca-open-new-card-input-text ca-new-card-full-name' and @type='text']")
    private WebElement cardholderFirstnameTextBox;

    @SVBFindBy(xpath = "//span[@class='label' and text()='Cardholder Last Name']")
    private WebElement cardholderLastNameTextBoxLabel;

    @SVBFindBy(xpath = "//input[@class='ca-input-text ca-open-new-card-input-text ca-new-card-last-name' and @type='text']")
    private WebElement cardholderLastNameTextBox;

    @SVBFindBy(xpath = "//input[@class='ca-input-text ca-open-new-card-input-text ca-new-card-ssn-text-value ca-new-card-ssn' and @type='text']")
    private WebElement cardholderSsnTextBox;

    @SVBFindBy(xpath = "//input[@class='svb-cc-date-input ca-date-input hasDatepicker ca-date-grey-text ca-dob-mask' and @type='text']")
    private WebElement cardholderDobTextBox;

    @SVBFindBy(xpath = "//input[@class='currency-text-field grayed-out' and @type='text']")
    private WebElement cardholderLimitTextBox;

    @SVBFindBy(xpath = "//input[@class='ca-input-text ca-open-new-card-input-text ca-new-card-email' and @type='text']")
    private WebElement cardholderEmailTextBox;

    @SVBFindBy(xpath = "//input[@class='ca-input-text ca-open-new-card-input-text ca-new-card-cell-phone' and @type='text']")
    private WebElement cardholderCellPhoneTextBox;

    @SVBFindBy(xpath = "//input[@class='ca-input-text ca-open-new-card-input-text ca-new-card-business-phone' and @type='text']")
    private WebElement cardholderBusPhoneTextBox;

    @SVBFindBy(xpath = "//label[@for='standard']")
    private WebElement cardDeliveryStdRadioBtn;

    @SVBFindBy(xpath = "//label[@for='expedited']")
    private WebElement cardDeliveryExpRadioBtn;

    @SVBFindBy(xpath = "//label[@for='yesCashAdvance']")
    private WebElement cashAdvanceYesRadioBtn;

    @SVBFindBy(xpath = "//label[@for='noCashAdvance']")
    private WebElement cashAdvanceNoRadioBtn;

    @SVBFindBy(xpath = "//input[@class='ca-input-text ca-open-new-card-input-text' and @type='text']")
    private WebElement cardholderAddInstructionTextBox;

    @SVBFindBy(xpath = "//input[@class='ca-input-text ca-open-new-card-input-text ca-new-card-name-on-card' and @type='text']")
    private WebElement nonPlasticAccountName;

    @SVBFindBy(xpath = "//button[@class=\"ca-button activeBlue next\"]")
    private WebElement nextBtn;

    CommonUtilities commonUtilities;
    WebDriver driver;

    public OpenNewCardPage() {
        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(DriverManager.getDriver(), this);
        commonUtilities = new CommonUtilities();
    }

    public void fillNewCardDetails(String cardFormat, String cardDelivery, String cashAdvance) {
        try {
            Thread.sleep(5 * 1000);
        } catch (Exception exception) {

        }

//        String tempUserName =
//        fakeTitleGenerator.name().username().replace(".", "");
//        String fakeFirstName = fakeTitleGenerator.name().fullName();
//        String fakeLastName = fakeTitleGenerator.name().lastName();
//        String fakeTitle = fakeTitleGenerator.job().title() + " " + fakeTitleGenerator.job().position();
//        String fakePhone = fakeTitleGenerator.phoneNumber().phoneNumber();
//        String fakeFaxNo = fakeTitleGenerator.phoneNumber().cellPhone();
//        String fakeEmail = tempUserName.trim() + "@svb.com";

        if ("Plastic".equals(cardFormat)) {
            plasticCardRadioBtn.click();
            cardholderFirstnameTextBox.sendKeys("Iron");
            cardholderLastNameTextBox.sendKeys("Man");
            cardholderSsnTextBox.sendKeys("0932");
            cardholderDobTextBox.sendKeys("04041988");
            cardholderLimitTextBox.sendKeys("100");
            cardholderEmailTextBox.sendKeys("ironman_automation@svb.com");
            cardholderCellPhoneTextBox.sendKeys("9996546547");
            cardholderBusPhoneTextBox.sendKeys("9999654654");
            cardDeliveryStdRadioBtn.click();
            cashAdvanceYesRadioBtn.click();
        }
        else if ("Non Plastic".equals(cardFormat)) {
            nonPlasticCardRadioBtn.click();
            nonPlasticAccountName.sendKeys("Iron" + (new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())));
            cardholderSsnTextBox.sendKeys("4444");
            cardholderLimitTextBox.sendKeys("100");
            cardholderEmailTextBox.sendKeys("ironman_automation@svb.com");
            cardholderCellPhoneTextBox.sendKeys("9996546547");
            cardholderBusPhoneTextBox.sendKeys("9999654654");
        }
        nextBtn.click();
    }
}

