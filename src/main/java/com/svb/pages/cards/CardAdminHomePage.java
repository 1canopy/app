package com.svb.pages.cards;

import com.svb.drivers.DriverCommand;
import com.svb.drivers.MuranoDashboardElements;
import com.svb.pages.HomePage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;

import com.svb.annotations.SVBFindBy;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.testng.Reporter;

import java.util.ArrayList;

public class CardAdminHomePage extends CardsHomePage {
	 private static Logger logger = LogManager.getLogger(CardAdminHomePage.class);
	 
	 /*@SVBFindBy(linkText = "Accounts")
	 private WebElement accountsLink;*/

    @SVBFindBy(xpath = MuranoDashboardElements.DEFAULT_MAIN_MENU_ACCOUNTS_LINK)
    private WebElement accountsLink;
	 
//	 @SVBFindBy(linkText = "Card Account Overview")
//	 private WebElement linkCardAccountOverview;

	@SVBFindBy(xpath = "//span[2]/div/div[3]/div[2]/a/ng-transclude/span")
	private WebElement linkCardAccountOverview;
	 
	 @SVBFindBy(xpath = "//span[@id='register-credit-card']")
	 private WebElement registerCreditCardButton;

	@SVBFindBy(xpath = "//svb-sidebar-business-credit-card-offer/div/div/h2")
	private WebElement labelBccOfferHeading;

	@SVBFindBy(xpath = "//svb-sidebar-business-credit-card-offer/div/div/div/div[2]/div/div[1]")
	private WebElement bodyBccOffer1;

	@SVBFindBy(xpath = "//svb-sidebar-business-credit-card-offer/div/div/div/div[2]/div/div[2]")
	private WebElement bodyBccOffer2;

	@SVBFindBy(xpath = "//*[@id='sidebarBusinessCreditCardId']/a")
	private WebElement learnMoreButton;
	 
	 CommonUtilities commonUtilities;
	 
	 public CardAdminHomePage()
	 {
		 CustomPageFactory.initElements(DriverManager.getDriver(), this);
		 commonUtilities = new CommonUtilities();
	 }
	 
	 public CardAccountOverviewPage navigateToCardAccountOverviewPage() throws InterruptedException {
		 commonUtilities.onMouseHoverAction(DriverManager.getDriver(), accountsLink, linkCardAccountOverview);
		 return new CardAccountOverviewPage();
	 }

	/* This method stores content for BCC Offer displayed
	 * on Card Admin Home page for eligible users
	 */
	public ArrayList<String> checkBccOfferDisplay()
	{
		ArrayList<String> bccOfferContent = new ArrayList<String>();
		bccOfferContent.add(labelBccOfferHeading.getText());
		bccOfferContent.add(bodyBccOffer1.getText());
		bccOfferContent.add(bodyBccOffer2.getText());
		bccOfferContent.add(learnMoreButton.getText());
		return bccOfferContent;
	}

	/* This method to take the screenshot of complete card overview
	 * screen by scrolling down to capture the BCC offer displayed.
	 */
	public void bccOfferDisplayedScreenshot()
	{
		if (labelBccOfferHeading.getText().equalsIgnoreCase("Recommendations"))
		{
			DriverCommand.takeScreenshot("BCC Overview Screen Offer Displayed - Test Evidence");
		}
		else
			Reporter.log("BCC Offer is not displayed for User");
	}

	/**
	 * Click Learn More Button
	 */
	public void clickLearnMoreButton()
	{
		learnMoreButton.click();
	}

}
