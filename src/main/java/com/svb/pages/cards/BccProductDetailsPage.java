package com.svb.pages.cards;

import com.svb.annotations.SVBFindBy;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import com.svb.pages.HomePage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;

public class BccProductDetailsPage {
	private static Logger logger = LogManager.getLogger(BccProductDetailsPage.class);

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[1]")
	private WebElement bccOfferDetailsTitle;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[2]/div")
	private WebElement bccOfferDetailsPhotoLabel;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[3]/div/div")
	private WebElement bccOfferDetailsBodyHeading1;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[3]/div/span")
	private WebElement bccOfferDetailsBodyContent1;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[4]/div[1]/div/div/h4")
	private WebElement bccOfferDetailsBodyHeading2;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[4]/div[1]/div/div/p")
	private WebElement bccOfferDetailsBodyContent2;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[4]/div[2]/div/div/h4")
	private WebElement bccOfferDetailsBodyHeading3;

	@SVBFindBy(xpath = "//div/p[contains(text(),'Credit that')]")
	private WebElement bccOfferDetailsBodyContent3;

//	@SVBFindBy(css = "")
//	private WebElement bccOfferDetailsBodyContent3;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[4]/div[3]/div/div/h4")
	private WebElement bccOfferDetailsBodyHeading4;

	@SVBFindBy(xpath = ".//*[@id='productOffers']/div/div[4]/div[3]/div/div/p[contains(text(),'Because you')]")
	private WebElement bccOfferDetailsBodyContent4;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[5]/div[1]/span")
	private WebElement applyNowBoxHeading;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[5]/div[2]/div[1]")
	private WebElement applyNowBoxBusinessNameLabel;

	@SVBFindBy(id = "showApplicantBusinessName")
	private WebElement applyNowBoxBusinessNameText;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[5]/div[3]/div[1]")
	private WebElement applyNowBoxApplicantNameLabel;

	@SVBFindBy(id = "showApplicantName")
	private WebElement applyNowBoxApplicantNameText;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[5]/div[4]/div[1]")
	private WebElement applyNowBoxApplicantContactEmail;

	@SVBFindBy(id = "showApplicantEmail")
	private WebElement applyNowBoxApplicantContactText;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[5]/div[6]/span")
	private WebElement applyNowBoxDetails;

	@SVBFindBy(id = "showTermsAndConditionLink")
	private WebElement termsLink;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[5]/div[5]/button")
	private WebElement applyNowButton;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[6]/div[1]/div/h4")
	private WebElement syncHeading;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[6]/div[1]/div/p")
	private WebElement syncContent;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[6]/div[2]/div/h4")
	private WebElement combineHeading;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[6]/div[2]/div/p")
	private WebElement combineContent;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[7]/div[1]/div/h4")
	private WebElement rewardHeading;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[7]/div[1]/div/p")
	private WebElement rewardContent;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[7]/div[2]/div/h4")
	private WebElement priceHeading;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[7]/div[2]/div/p[contains(text(),'No membership')]")
	private WebElement priceContent;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[8]/div[1]/ul/li[1]")
	private WebElement footNote1Content;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[8]/div[1]/ul/li[2]")
	private WebElement footNote2Content;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[8]/div[1]/ul/li[3]")
	private WebElement footNote3Content;

	@SVBFindBy(xpath = "//*[@id='productOffers']/div/div[8]/div[2]/div")
	private WebElement footNote4Content;

	@SVBFindBy(id = "logoutBtn")
	private WebElement logoutButton;

	CommonUtilities commonUtilities;

	 public BccProductDetailsPage()
	 {
		 CustomPageFactory.initElements(DriverManager.getDriver(), this);
		 commonUtilities = new CommonUtilities();
	 }

	/**
	 * Method returns product details content in an array
	 * @return
	 */
	public ArrayList<String> checkBccProductDetailsDisplay() {
		ArrayList<String> bccProductDetailsContent = new ArrayList<String>();
		bccProductDetailsContent.add(bccOfferDetailsTitle.getText());
		bccProductDetailsContent.add(bccOfferDetailsPhotoLabel.getText());
		bccProductDetailsContent.add(bccOfferDetailsBodyHeading1.getText());
		bccProductDetailsContent.add(bccOfferDetailsBodyContent1.getText());
		bccProductDetailsContent.add(bccOfferDetailsBodyHeading2.getText());
		bccProductDetailsContent.add(bccOfferDetailsBodyContent2.getText());
		bccProductDetailsContent.add(bccOfferDetailsBodyHeading3.getText());
		bccProductDetailsContent.add(bccOfferDetailsBodyContent3.getText());
		bccProductDetailsContent.add(bccOfferDetailsBodyHeading4.getText());
		bccProductDetailsContent.add(bccOfferDetailsBodyContent4.getText());
		bccProductDetailsContent.add(applyNowBoxHeading.getText());
		bccProductDetailsContent.add(applyNowBoxBusinessNameLabel.getText());
		bccProductDetailsContent.add(applyNowBoxBusinessNameText.getText());
		bccProductDetailsContent.add(applyNowBoxApplicantNameLabel.getText());
		bccProductDetailsContent.add(applyNowBoxApplicantNameText.getText());
		bccProductDetailsContent.add(applyNowBoxApplicantContactEmail.getText());
		bccProductDetailsContent.add(applyNowBoxApplicantContactText.getText());
		bccProductDetailsContent.add(applyNowBoxDetails.getText());
		bccProductDetailsContent.add(termsLink.getText());
		bccProductDetailsContent.add(applyNowButton.getText());
		bccProductDetailsContent.add(syncHeading.getText());
		bccProductDetailsContent.add(syncContent.getText());
		bccProductDetailsContent.add(combineHeading.getText());
		bccProductDetailsContent.add(combineContent.getText());
		bccProductDetailsContent.add(rewardHeading.getText());
		bccProductDetailsContent.add(rewardContent.getText());
		bccProductDetailsContent.add(priceHeading.getText());
		bccProductDetailsContent.add(priceContent.getText());
		bccProductDetailsContent.add(footNote1Content.getText());
		bccProductDetailsContent.add(footNote2Content.getText());
		bccProductDetailsContent.add(footNote3Content.getText());
		bccProductDetailsContent.add(footNote4Content.getText());
		return bccProductDetailsContent;
	}

	public void logoutSession() {
		logoutButton.click();
	}
    
}
