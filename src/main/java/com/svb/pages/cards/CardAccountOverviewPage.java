package com.svb.pages.cards;

import com.svb.annotations.SVBFindBy;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import com.svb.pages.HomePage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CardAccountOverviewPage extends CardsHomePage {
    private static Logger logger = LogManager.getLogger(CardAccountOverviewPage.class);

//	@SVBFindBy(xpath = "//div[@class='dropdown-selectValue' and contains(text(),'More Actions')]")
//	private WebElement moreActions;

    @SVBFindBy(xpath = "//div[@class=\"dropdown-selectValue\" and contains(text(),\"More Actions\")]")
    private WebElement moreActions;

    @SVBFindBy(xpath = "//div[@class='downArrow']")
    private WebElement downArrow;

    @SVBFindBy(xpath = "//span[@data-bind=\"html: $data.value\" and contains(text(),\"Open a New Card\")]")
    private WebElement openNewCardLink;

    @SVBFindBy(xpath = "//div[4]/div/span[1]/span[1]/label/span")
    private WebElement subCardList4;

    @SVBFindBy(xpath = "//button[@class=\"change-status\"]")
    private WebElement changeStatusBtn;

    WebDriver driver;
    CommonUtilities commonUtilities;

    public CardAccountOverviewPage() {
        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(DriverManager.getDriver(), this);
        commonUtilities = new CommonUtilities();
    }

    /**
     * @param moreActionOption
     */
    public void selectMoreActions(String moreActionOption) {
        //       commonUtilities.waitForElementPresent(driver,By.xpath("//div[@class='dropdown-selectValue' and contains(text(),'More Actions')]") );
//        commonUtilities.waitForElementPresent(driver,By.xpath("//dropdown-widget/div/label/div/div[2]") );
        Select moreActionsDropDown = commonUtilities.selectByDropDownValueElement(driver, moreActions);
        DriverCommand.takeScreenshot("Selecting More Actions:" + moreActionOption + "\" - Test Evidence");
        moreActionsDropDown.selectByVisibleText(moreActionOption);
    }

    /**
     * Method click more actions dropdown and clicks Open New Card Page
     */
    public void clickOpenNewCardLink() {
        // boolean isElementPresentToClick = commonUtilities.waitForElementToBeClickable(DriverManager.getDriver(), By.xpath("//div[@class=\"dropdown-selectValue\" and contains(text(),\"More Actions\")]"));
        boolean isElementPresentToClick = true;
        try {
            Thread.sleep(15 * 1000);
        } catch (Exception exception) {

        }
        if (isElementPresentToClick) {
            moreActions.click();
        }
        DriverCommand.takeScreenshot("Selecting More Actions: New Card Request- Test Evidence");
        openNewCardLink.click();
    }

    public void clickWhenReady(By locator, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void clickSubCardInList(int cardNumber){
        try {
            Thread.sleep(15 * 1000);
        } catch (Exception exception) {

        }
        subCardList4.click();
    }

    public void clickChangeStatus(){
        changeStatusBtn.click();
    }


}
