package com.svb.pages.cards;

import com.svb.annotations.SVBFindBy;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import com.svb.pages.HomePage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import java.util.ArrayList;

public class CardBasePage  {
	 private static Logger logger = LogManager.getLogger(CardBasePage.class);

	 @SVBFindBy(xpath = "//a[@id=\"headerMenuId1\" and contains(text(),\"Accounts\")]")
	 private WebElement accountsLink;

	 @SVBFindBy(linkText = "Card Account Overview")
	 private WebElement linkCardAccountOverview;

	 @SVBFindBy(xpath = "//span[@id='register-credit-card']")
	 private WebElement registerCreditCardButton;



	 CommonUtilities commonUtilities;

	 public CardBasePage()
	 {
		 CustomPageFactory.initElements(DriverManager.getDriver(), this);
		 commonUtilities = new CommonUtilities();
	 }
	 
	 public CardAccountOverviewPage navigateToCardAccountOverviewPage() throws InterruptedException {
		 commonUtilities.onMouseHoverAction(DriverManager.getDriver(), accountsLink, linkCardAccountOverview);
		 return new CardAccountOverviewPage();
	 }

}
