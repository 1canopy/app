package com.svb.pages.cards;

import com.svb.annotations.SVBFindBy;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CardStatusChangePage {
    private static Logger logger = LogManager.getLogger(CardStatusChangePage.class);

    @SVBFindBy(xpath = "//div[@class=\"wrapper-dropdown\"]")
    private WebElement stausChangeDropdown;

    @SVBFindBy(xpath = "//li[@data-bind=\"event:{click: $data.disableCardOption() == true ? '' : $parent.optionClicked}, attr:{optionid: $data.id},css:{'disable-list-item': $data.disableCardOption()}\" and @optionid=\"3\"]")
    private WebElement selectSuspended;

    @SVBFindBy(xpath = "//button[@data-bind=\"event:{click: $data.disableCardOption() == true ? '' : $parent.optionClicked}, attr:{optionid: $data.id},css:{'disable-list-item': $data.disableCardOption()}\" and @optionid=\"2\"]")
    private WebElement selectClosed;

    @SVBFindBy(id = "nextButton")
    private WebElement nextBtn;

    @SVBFindBy(xpath = "//button[@class=\"ca-button activeBlue confirmbtn\"]")
    private WebElement confirmBtn;

    CommonUtilities commonUtilities;
    WebDriver driver;

    public CardStatusChangePage() {
        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(DriverManager.getDriver(), this);
        commonUtilities = new CommonUtilities();
    }

    public void selectCardStatusChangeInDropdown(String statusChangeType){
        try{
            Thread.sleep(5* 1000);
        } catch (Exception exception) {

        }
        if ("Suspended".equals(statusChangeType)){
            stausChangeDropdown.click();
            selectSuspended.click();}
            else if ("Closed".equals(statusChangeType)){
            stausChangeDropdown.click();
        }
    }

    public void clickNext(){
        nextBtn.click();
    }

    public void clickConfirm(){
        confirmBtn.click();
    }

}

