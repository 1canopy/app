package com.svb.pages;

import com.svb.annotations.SVBFindBy;
import com.svb.annotations.Until;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UserAdministrationPage {

    @SVBFindBy(xpath = "//a[contains(text(),\"Maintain Client Authenticate\")]")
    private WebElement linkClientAuthentication;

    @SVBFindBy(until = Until.VISIBLE, timeout = 5, xpath = "//span[contains(text(),'Client Administration') and @class='pageTitle']")
    private WebElement currentPageHeading;

    @SVBFindBy(xpath = "//td[@class='pageTitle']")
    private WebElement userAdminAddUserPageHeader;

    WebDriver driver;
    CommonUtilities commonUtilities;

    public UserAdministrationPage() {
        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(DriverManager.getDriver(), this);
        commonUtilities = new CommonUtilities();
    }

    public MaintainClientAuthenticatePage navigateToClientAuthentication() {
        DriverCommand.takeScreenshot("Navigated to \"User Administration\" Page - Test Evidence");
        linkClientAuthentication.click();
        DriverCommand.takeScreenshot("Navigated to \"Maintain Client Authenticate\" link clicked - Test Evidence");
        return new MaintainClientAuthenticatePage();
    }

    public String getHeadingText() {
        return currentPageHeading.getText();
    }

    public boolean isUserAdministrationPageDisplayed() {
        return currentPageHeading.isDisplayed();
    }

}
