package com.svb.pages;

import com.svb.annotations.SVBFindBy;
import com.svb.annotations.Until;
import com.svb.constants.SearchByClient;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SwitchClientDialog {

    @SVBFindBy(until = Until.VISIBLE, timeout = 10, xpath = "//span[contains(text(),\"Please Select a Client.\")]")
    private WebElement selectClientDialogHeading;

    @SVBFindBy(until = Until.VISIBLE, timeout = 5, id = "getClients")
    private WebElement searchClientButton;

    @SVBFindBy(until = Until.VISIBLE, timeout = 10, xpath = "//input[@id='searchBy' and @type='radio']/following-sibling::font[contains(text(),'Account Number')]")
    private WebElement searchByAccNumberRadioButton;

    @SVBFindBy(until = Until.VISIBLE, timeout = 10, xpath = "//input[@id='searchBy' and @type='radio']/following-sibling::font[contains(text(),'User ID')]")
    private WebElement searchByUserIDRadioButton;

    @SVBFindBy(until = Until.VISIBLE, timeout = 10, xpath = "//input[@id='searchBy' and @type='radio']/following-sibling::font[contains(text(),'Company ID (Cards)')]")
    private WebElement searchByCompanyIDRadioButton;

    @SVBFindBy(until = Until.VISIBLE, timeout = 10, xpath = "//input[@id='searchBy' and @type='radio']/following-sibling::font[contains(text(),'Client Name')]")
    private WebElement searchByClientNameRadioButton;

    @SVBFindBy(until = Until.VISIBLE, timeout = 5, id = "clientSearchString")
    private WebElement inputClientSearchString;

    WebDriver driver;
    CommonUtilities commonUtilities;

    public SwitchClientDialog() {
        this.driver = DriverManager.getDriver();
        CustomPageFactory.initElements(driver, this);
        commonUtilities = new CommonUtilities();
    }

    private void selectSearchByClientCriteria(String searchBy) {
        if (searchBy.trim().equalsIgnoreCase(SearchByClient.CLIENT_NAME.getSearchBy())) {
            searchByClientNameRadioButton.click();
        } else if (searchBy.trim().equalsIgnoreCase(SearchByClient.ACCOUNT_NUMBER.getSearchBy())) {
            searchByAccNumberRadioButton.click();
        } else if (searchBy.trim().equalsIgnoreCase(SearchByClient.USER_ID.getSearchBy())) {
            searchByUserIDRadioButton.click();
        } else if (searchBy.trim().equalsIgnoreCase(SearchByClient.COMPANY_ID.getSearchBy())) {
            searchByCompanyIDRadioButton.click();
        }
    }

    public void selectAndSwitchClient(String searchBy, String clientSearchString) {
        selectSearchByClientCriteria(searchBy);
        if (null != clientSearchString) {
            inputClientSearchString.sendKeys(clientSearchString);
            DriverCommand.takeScreenshot("Select/Switch Client dialog window - Search By : \"" + searchBy + "\" Radio Button Selection - Test Evidence");
            WebElement searchClientButton = commonUtilities.waitForElementPresent(this.driver, By.id("getClients"));
            searchClientButton.click();

            DriverCommand.takeScreenshot("Client Search Result - Test Evidence");
            WebElement submitButton = commonUtilities.waitForElementPresent(this.driver, By.id("submitButton"));
            submitButton.click();
            WebElement displayClient = commonUtilities.waitForElementPresent(this.driver, By.xpath("//div[@id=\"displayClient\"]/div[contains(text(),\"Selected Client\")]"));
            displayClient.click();
            DriverCommand.takeScreenshot("Select/Switch Client dialog window - Client Search - Test Evidence");
        }
    }
}
