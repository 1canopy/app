package com.svb.pages;

import com.svb.annotations.SVBFindBy;
import com.svb.annotations.Until;
import com.svb.constants.ServiceType;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

/**
 * @author dikuppan
 */
public class ModifyClientServicesPage {

    private static Logger logger = LogManager.getLogger(ModifyClientServicesPage.class);

    private static final CommonUtilities commonUtilities = new CommonUtilities();

    @SVBFindBy(name = "ukServiceSelected")
    private WebElement ukServiceSelected;

    @SVBFindBy(name = "usServiceSelected")
    private WebElement usServiceSelected;

    // Value = 1 is for "US"; Value = 2 is for "UK"
    @SVBFindBy(name = "administeredClient.primaryAffiliation")
    private WebElement primaryAffiliation;

    // True/False
    @SVBFindBy(name = "isDualAdminEnabled")
    private WebElement dualAdministration;

    @SVBFindBy(xpath = "//img[@src='/images/continue.jpg']")
    private WebElement continueButton;

    @SVBFindBy(xpath = "//img[@src='/images/submit.jpg']")
    private WebElement submitButton;

    @SVBFindBy(until = Until.VISIBLE, timeout = 5, xpath = "//span[@class='pageTitle']/b[contains(text(),'Client Administration Client Information: Modify Client Services & Billing Information')]")
    private WebElement headingLabel;

    public ModifyClientServicesPage() {
        CustomPageFactory.initElements(DriverManager.getDriver(), this);
    }

    public void enrollServices(String serviceType) {
        DriverCommand.takeScreenshot("Navigated to \"Client Administration:Modify Services & Billing Information \" Page - Test Evidence");
        if (ServiceType.UK_SERVICES.getServicesType().equalsIgnoreCase(serviceType)) {
            commonUtilities.selectCheckBox(ukServiceSelected);
        } else if (ServiceType.US_SERVICES.getServicesType().equalsIgnoreCase(serviceType)) {
            commonUtilities.selectCheckBox(usServiceSelected);
        } else if (ServiceType.ALL.getServicesType().equalsIgnoreCase(serviceType)) {
            commonUtilities.selectCheckBox(usServiceSelected);
            commonUtilities.selectCheckBox(ukServiceSelected);
        } else {
            commonUtilities.selectCheckBox(usServiceSelected);
        }
        DriverCommand.takeScreenshot("Client Modified Services/Entitlements page - Test Evidence");
        continueButton.click();
        DriverCommand.takeScreenshot("Client Services/Entitlements Preview Page before submitting - Test Evidence");
        submitButton.click();
        DriverCommand.takeScreenshot("Saved the changes successfully - Client Confirmation Page - Test Evidence");
    }

    /**
     * Gets Current Page Header text
     * Usage : For Assertion
     *
     * @return
     */
    public String getHeadingText() {
        return headingLabel.getText();
    }

    /**
     * Check for Client Administration
     *
     * @return
     * @throws NoSuchElementException
     */
    public boolean isClientAdministrationPageDisplayed() throws NoSuchElementException {
        return headingLabel.isDisplayed();
    }

    /**
     * Checks for whether the Client is enabled for "Dual Administration"
     *
     * @return
     * @throws NoSuchElementException
     */
    public boolean isDualAdminEnabledClient() throws NoSuchElementException {
        return dualAdministration.isEnabled();
    }

}
