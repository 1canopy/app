package com.svb.pages;

import com.svb.annotations.SVBFindBy;
import com.svb.drivers.BasePageSetup;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.openqa.selenium.WebElement;

public class LoginPage {

    @SVBFindBy(id = "userId")
    private WebElement usernameTextField;

    @SVBFindBy(id = "userPassword")
    private WebElement passwordTextField;

    @SVBFindBy(id = "loginButton")
    private WebElement loginButton;

    public LoginPage() {
        CustomPageFactory.initElements(DriverManager.getDriver(), this);
    }

    public ConfirmIdentityModal login(String username, String password) {
        usernameTextField.sendKeys(username);
        passwordTextField.sendKeys(password);
        DriverCommand.takeScreenshot("Login Screen - Test Evidence");
        loginButton.click();
        return new ConfirmIdentityModal();
    }

}
