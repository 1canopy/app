package com.svb.pages;

import com.svb.annotations.SVBFindBy;
import com.svb.annotations.Until;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class StatementsPage {

    private static Logger logger = LogManager.getLogger(StatementsPage.class);

    WebDriver driver;
    CommonUtilities commonUtilities;

    @SVBFindBy(until = Until.VISIBLE, timeout = 5, linkText = "Statements")
    private WebElement statementPeriod;

    public StatementsPage() {
        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(driver, this);
        commonUtilities = new CommonUtilities();
    }

    public StatementsResultsPage clickStatementsHyperLink() {
        // WebElement linkStatements = commonUtilities.waitForElementPresent(this.driver, By.linkText("Statements"));
        //linkStatements.click();
        statementPeriod.click();
        DriverCommand.takeScreenshot("\"Statements Selection\" Page Displayed - Test Evidence");
        return new StatementsResultsPage();
    }

}
