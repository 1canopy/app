package com.svb.pages;

import com.svb.annotations.SVBFindBy;
import com.svb.annotations.Until;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.Map;

public class ConfirmIdentityModal {

    @SVBFindBy(id = "modal-window-id", until = Until.VISIBLE, timeout = 10)
    private WebElement identityModal;

    @SVBFindBy(className = "svb-modal-confirm-identity-label-container")
    private WebElement questionLabel;

    @SVBFindBy(id = "enteredChallengePhraseResponse")
    private WebElement answerTextField;

    @SVBFindBy(xpath = "//*[@id='modal-window-id']/footer/button")
    private WebElement confirmIdentityButton;

    public ConfirmIdentityModal() {
        CustomPageFactory.initElements(DriverManager.getDriver(), this);
    }

    public boolean isIdentityModalDisplayed() {
        return identityModal.isDisplayed();
    }

    private boolean performIdentityChallenge() {
        String question = questionLabel.getText().trim();
        String match = matchQuestion(question);
        if (match == null) {
            return false;
        }
        if (match.equals("childhood") || match.equals("childhood1")) {
            //answerTextField.sendKeys("childhood");
            match = "childhood";
        }
        answerTextField.sendKeys(match);
        DriverCommand.takeScreenshot("Confirm Identity Modal - Test Evidence");
        confirmIdentityButton.click();
        return true;
    }

    public IdentityConfirmedModal handleConfirmIdentityModal() {
        if (performIdentityChallenge()) {
            return new IdentityConfirmedModal();
        }
        return null;
    }

    private String matchQuestion(String question) {
        Map<String, String> questionAnswerMap = new HashMap<>();
        questionAnswerMap.put("city", "What is the city of your birth?");
        questionAnswerMap.put("color", "What is your favorite color?");
        questionAnswerMap.put("name", "What is your pet's name?");
        questionAnswerMap.put("car", "What was the make/model of your first car?");
        questionAnswerMap.put("hobby", "What is your favorite hobby?");
        questionAnswerMap.put("movie", "What is your all-time favorite movie?");
        questionAnswerMap.put("dessert", "What is your favorite dessert?");
        questionAnswerMap.put("actor", "Who is your favorite actor or musician?");
        questionAnswerMap.put("architect", "Who is your favorite architect?");
        questionAnswerMap.put("childhood", "What was your favorite childhood hiding place?");
        questionAnswerMap.put("beverage", "What is your favorite beverage?");
        questionAnswerMap.put("novel", "What is your all-time favorite novel or story?");
        questionAnswerMap.put("childhood1", "What was your favorite childhood game?");
        questionAnswerMap.put("tree", "What is your favorite tree or flower?");
        questionAnswerMap.put("restaurant", "Where is your favorite restaurant located?");
        questionAnswerMap.put("band", "What is your favorite band?");
        questionAnswerMap.put("job", "What was your first job?");
        questionAnswerMap.put("sports", "What is your favorite sports team?");
        questionAnswerMap.put("pet", "What is the name of your first pet?");
        questionAnswerMap.put("school", "What was the name of your first school?");
        for (Map.Entry<String, String> questionItem : questionAnswerMap.entrySet()) {
            if (questionItem.getValue().equals(question)) {
                return questionItem.getKey();
            }
        }
        return null;
    }

}
