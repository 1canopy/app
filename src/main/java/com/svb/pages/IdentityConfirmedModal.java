package com.svb.pages;

import com.svb.annotations.SVBFindBy;
import com.svb.annotations.Until;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.openqa.selenium.WebElement;

public class IdentityConfirmedModal {

    @SVBFindBy(xpath = "//h2[contains(text(), 'Identity Confirmed')]", until = Until.VISIBLE, timeout = 10)
    private WebElement identityConfirmedModal;

    @SVBFindBy(xpath = "//*[@id='modal-window-id']/footer/button[contains(text(), 'Continue to Online Banking')]")
    private WebElement continueOnlineButton;

    public IdentityConfirmedModal() {
        CustomPageFactory.initElements(DriverManager.getDriver(), this);
    }

    public boolean isIdentityConfirmedModalDisplayed() {
        return identityConfirmedModal.isDisplayed();
    }

    public HomePage handleIdentityConfirmed() {
        continueOnlineButton.click();
        return new HomePage();
    }

}