package com.svb.pages;

import com.github.javafaker.Faker;
import com.svb.annotations.SVBFindBy;
import com.svb.annotations.Until;
import com.svb.constants.BankUserGroupType;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ManageUserAdministration {

    private static Logger logger = LogManager.getLogger(ManageUserAdministration.class);
    private static final Faker fakeTitleGenerator = new Faker();

    @SVBFindBy(until = Until.VISIBLE, timeout = 5, xpath = "//td[contains(text(),'User Administration') and @class='pageTitle']")
    private WebElement currentPageHeading;

    @SVBFindBy(xpath = "//input[@name='Get Users' and @value='Get Users' and @type='button']")
    private WebElement getUsersButton;

    @SVBFindBy(xpath = "//button[@id='addUser']")
    private WebElement addUser;

    @SVBFindBy(linkText = "Cancel")
    private WebElement cancelButton;

    @SVBFindBy(xpath = "//button[@id='Continue' and @type='button']")
    private WebElement continueButton;

    @SVBFindBy(xpath = "//button[@id='Clear' and @type='button']")
    private WebElement clearButton;

    @SVBFindBy(id = "firstName")
    private WebElement firstName;

    @SVBFindBy(id = "loginName")
    private WebElement loginName;

    @SVBFindBy(id = "lastName")
    private WebElement lastName;

    @SVBFindBy(id = "title")
    private WebElement title;

    @SVBFindBy(id = "phone")
    private WebElement phone;

    @SVBFindBy(id = "fax")
    private WebElement faxNo;

    @SVBFindBy(id = "email")
    private WebElement email;

    @SVBFindBy(id = "groupID")
    private WebElement groupID;

    @SVBFindBy(xpath = "//button[@id='Submit']")
    private WebElement submitButton;

    @SVBFindBy(id = "Level1-allSvcs")
    private WebElement allServices;

    @SVBFindBy(xpath = "//td[@class='bodyTextBoldSVBMaroon' and contains(text(),'Set-up Confirmed')]")
    private WebElement setUpConfirmed;

    WebDriver driver;
    CommonUtilities commonUtilities;

    public ManageUserAdministration() {
        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(driver, this);
        commonUtilities = new CommonUtilities();
    }

    /**
     * @param userGroupType
     */
    public void selectUserGroup(String userGroupType) {

        String tempUserName = fakeTitleGenerator.name().fullName();

        Select queryCategoryTypeDropDown = commonUtilities.selectByDropDownValue(this.driver, By.xpath("//select[@name='bankUserGroupId']"));
        queryCategoryTypeDropDown.selectByVisibleText(userGroupType);

        DriverCommand.takeScreenshot("Selected : \"Bank User Group\" - \"" + userGroupType + "\" - Test Evidence");
        getUsersButton.click();
    }

    public void addNewUser() {
        addUser.click();
        DriverCommand.takeScreenshot("Navigated to \"Maintain Client Authenticate\" link clicked - Test Evidence");
    }

    public void fillUserDetails() {
        fillUserDetails(BankUserGroupType.CASH_MANAGEMENT_SERVICES.getUserRoleType());
    }

    public void fillUserDetails(String userType) {

        String tempUserName = fakeTitleGenerator.name().username().replace(".", "");
        String fakeFirstName = fakeTitleGenerator.name().fullName();
        String fakeLastName = fakeTitleGenerator.name().lastName();
        String fakeTitle = fakeTitleGenerator.job().title() + " " + fakeTitleGenerator.job().position();
        String fakePhone = fakeTitleGenerator.phoneNumber().phoneNumber();
        String fakeFaxNo = fakeTitleGenerator.phoneNumber().cellPhone();
        String fakeEmail = tempUserName.trim() + "@svb.com";

        loginName.sendKeys(tempUserName);
        firstName.sendKeys(fakeFirstName);
        lastName.sendKeys(fakeLastName);
        title.sendKeys(fakeTitle);
        phone.sendKeys(fakePhone);
        faxNo.sendKeys(fakeFaxNo);
        email.sendKeys(fakeEmail);
        selectBankUserGroup(By.id("groupID"), userType);
        continueButton.click();
    }

    public void selectAllServicesAndSubmit() {
        if (!allServices.isSelected()) {
            allServices.click();
        }
        DriverCommand.takeScreenshot("Services assigned for the User - Test Evidence");
        continueButton.click();
    }

    public void verifyServicesAssignedAndApprove() {
        DriverCommand.takeScreenshot("Approve the users Services - Test Evidence");
        submitButton.click();
    }

    public String setUpConfirmed() {
        DriverCommand.takeScreenshot("New/Modify User Set-Up Confirmed - Test Evidence");
        return setUpConfirmed.getText().trim();
    }

    /**
     * @param byElement
     * @param selectVisibleText
     */
    public void selectBankUserGroup(By byElement, String selectVisibleText) {
        Select queryCategoryTypeDropDown = commonUtilities.selectByDropDownValue(this.driver, byElement);
        queryCategoryTypeDropDown.selectByVisibleText(selectVisibleText);
        DriverCommand.takeScreenshot("Selected : \"Bank User Group\" - \"" + selectVisibleText + "\" - Test Evidence");
    }

    public String getHeadingText() {
        return currentPageHeading.getText();
    }
}
