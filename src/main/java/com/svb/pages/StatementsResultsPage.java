package com.svb.pages;

import com.svb.annotations.SVBFindBy;
import com.svb.annotations.Until;
import com.svb.constants.AccountReportingEnum;
import com.svb.drivers.CommonUtilities;
import com.svb.drivers.DriverCommand;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class StatementsResultsPage {

    @SVBFindBy(until = Until.VISIBLE, timeout = 5, xpath = "//td[@class='pageTitle' and contains(text(),'Statements - Selection Criteria')]")
    private WebElement headingLabel;

    @SVBFindBy(until = Until.VISIBLE, timeout = 5, xpath = "//select[@name='statementPeriod']")
    private WebElement statementPeriod;

    @SVBFindBy(until = Until.VISIBLE, timeout = 5, xpath = "//select[@name='selectedStatementType']")
    private WebElement selectedStatementType;

    @SVBFindBy(until = Until.VISIBLE, timeout = 5, name = "submitform")
    private WebElement saveButton;

    @SVBFindBy(until = Until.VISIBLE, timeout = 5, name = "cancel")
    private WebElement cancelButton;

    WebDriver driver;
    CommonUtilities commonUtilities;

    public StatementsResultsPage() {
        driver = DriverManager.getDriver();
        CustomPageFactory.initElements(driver, this);
        commonUtilities = new CommonUtilities();
    }

    public String getHeadingText() {
        return headingLabel.getText();
    }

    /**
     * @param statementPeriodValue
     * @param statementTypeValue
     */
    public void statementSelectionCriteria(String statementPeriodValue, String statementTypeValue) {

        Select statementPeriodDropDown = commonUtilities.selectByDropDownValue(this.driver, By.xpath("//select[@name='statementPeriod']"));
        statementPeriodDropDown.selectByVisibleText(statementPeriodValue);

        Select statementTypeDropDown = commonUtilities.selectByDropDownValue(this.driver, By.xpath("//select[@name='selectedStatementType']"));
        statementTypeDropDown.selectByVisibleText(statementTypeValue);

        Select selectedAcctGrpAndIdentifier = commonUtilities.selectByDropDownValue(this.driver, By.name("selectedAcctGrpAndIdentifier"));
        selectedAcctGrpAndIdentifier.selectByVisibleText("All Deposit Accounts");

        DriverCommand.takeScreenshot("Statement Selection - Search Criteria - Test Evidence");
        WebElement searchStatementsButton = commonUtilities.waitForElementPresent(this.driver, By.name("submitform"));
        searchStatementsButton.click();

        WebElement verifyPageTitle = commonUtilities.waitForElementPresent(this.driver, By.xpath("//td[@class='pageTitle' and contains(text(),'Statements')]"));
        Assert.assertEquals(verifyPageTitle.getText(), AccountReportingEnum.STATEMENTS.getAccountReporting());

        DriverCommand.takeScreenshot("Statement Selection - Statements Results - Test Evidence");

    }

}
