package com.svb.pages;

import com.svb.annotations.SVBFindBy;
import com.svb.drivers.DriverManager;
import com.svb.factory.CustomPageFactory;
import org.openqa.selenium.WebElement;

import java.util.List;

public class CheckboxPage {
    @SVBFindBy(xpath = "//*[@id='content']/div/h3")
    private WebElement headingLabel;

    @SVBFindBy(xpath = "//*[@id='checkboxes']/input")
    private List<WebElement> checkboxes;

    public CheckboxPage() {
        CustomPageFactory.initElements(DriverManager.getDriver(), this);
    }

    public String getHeadingLabelText() {
        return headingLabel.getText();
    }

    public boolean isCheckboxChecked(int index) {
        return checkboxes.get(index).isSelected();
    }
}
