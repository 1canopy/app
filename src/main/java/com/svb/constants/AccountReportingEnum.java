package com.svb.constants;

/**
 * Create Query Page - Account Reporting Type
 */
public enum AccountReportingEnum {

    ACCOUNT_DETAILS("Account Details"),
    TRANSACTION_DETAILS("Transaction Details"),
    DOWNLOAD_DATA("Download Data"),
    SPECIAL_REPORTS("Special Reports"),
    STATEMENTS("Statements");

    private String accountReporting;

    AccountReportingEnum(String accountReporting) {
        this.accountReporting = accountReporting;
    }

    public String getAccountReporting() {
        return accountReporting;
    }
}
