package com.svb.constants;

public enum ClientAuthStatus {

    ENABLE("Enable"),
    DISABLE("Disable");

    private String status;

    ClientAuthStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
