package com.svb.constants;

public enum SearchByClient {

    CLIENT_NAME("Client Name"),
    ACCOUNT_NUMBER("Account Number"),
    USER_ID("User ID"),
    COMPANY_ID("Company ID (Cards)");

    private String searchBy;

    SearchByClient(String searchBy) {
        this.searchBy = searchBy;
    }

    public String getSearchBy() {
        return searchBy;
    }
}
