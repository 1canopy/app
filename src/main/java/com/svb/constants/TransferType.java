package com.svb.constants;

public enum TransferType {
    ONETIME("One-time"),
    RECURRING("Recurring");

    private String scheduleTransferTypeVal;

    TransferType(String scheduleTransferTypeVal) {
        this.scheduleTransferTypeVal = scheduleTransferTypeVal;
    }

    public String getTransferType() {
        return scheduleTransferTypeVal;
    }
}
