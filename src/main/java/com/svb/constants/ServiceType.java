package com.svb.constants;

/**
 * @author dikuppan
 */
public enum ServiceType {
    UK_SERVICES("UK"),
    US_SERVICES("US"),
    ALL("ALL");

    private String servicesType;

    ServiceType(String servicesType) {
        this.servicesType = servicesType;
    }

    public String getServicesType() {
        return servicesType;
    }
}
