package com.svb.constants;

public enum BankUserGroupType {

    CASH_MANAGEMENT_SERVICES("Cash Management Services"),
    CLIENT_SERVICE_OFFICERS("Client Service Officers"),
    ONLINE_CHANNEL("Online Channel"),
    LOAN_PROCESSING("Loan Processing"),
    INTERNATIONAL("International"),
    APPLICATION_SUPPORT("Application Support"),
    OPERATIONS("Operations"),
    BASIC_INQUIRY("Basic Inquiry"),
    CLIENT_ADVISORY_SERVICES("Client Advisory Services"),
    INFORMATION_TECHNOLOGY("Information Technology"),
    OPERATIONS_LEVEL_I("Operations Level I"),
    OPERATIONS_LEVEL_II("Operations Level II"),
    GLOBAL_FX("Global Funds Transfer/FX Operations/FX Traders"),
    GLOBAL_DEPOSITS("Global Deposits"),
    EFI("E-FRaud Investigations Unit"),
    CARD_CLIENT_SUPPORT("Card Client Support"),
    CARD_OPERATIONS("Card Operations"),
    FIRST_DATA_SUPPORT("First Data Support");

    private String userRoleType;

    BankUserGroupType(String userRoleType) {
        this.userRoleType = userRoleType;
    }

    public String getUserRoleType() {
        return userRoleType;
    }

}
