package com.svb.constants;

/**
 * {link} - https://www.codeproject.com/Tips/351122/What-is-software-testing-What-are-the-different-ty
 * Types of testing
 * There are many types of testing like
 * <p>
 * Unit Testing
 * Integration Testing
 * Functional Testing
 * System Testing
 * Stress Testing
 * Performance Testing
 * Usability Testing
 * Acceptance Testing
 * Regression Testing
 * Beta Testing
 * <p>
 * Unit Testing
 * Unit testing is the testing of an individual unit or group of related units. It falls under the class of white box testing. It is often done by the programmer to test that the unit he/she has implemented is producing expected output against given input.
 * <p>
 * Integration Testing
 * Integration testing is testing in which a group of components are combined to produce output. Also, the interaction between software and hardware is tested in integration testing if software and hardware components have any relation. It may fall under both white box testing and black box testing.
 * <p>
 * Functional Testing
 * Functional testing is the testing to ensure that the specified functionality required in the system requirements works. It falls under the class of black box testing.
 * <p>
 * System Testing
 * System testing is the testing to ensure that by putting the software in different environments (e.g., Operating Systems) it still works. System testing is done with full system implementation and environment. It falls under the class of black box testing.
 * <p>
 * Stress Testing
 * Stress testing is the testing to evaluate how system behaves under unfavorable conditions. Testing is conducted at beyond limits of the specifications. It falls under the class of black box testing.
 * <p>
 * Performance Testing
 * Performance testing is the testing to assess the speed and effectiveness of the system and to make sure it is generating results within a specified time as in performance requirements. It falls under the class of black box testing.
 * <p>
 * Usability Testing
 * Usability testing is performed to the perspective of the client, to evaluate how the GUI is user-friendly? How easily can the client learn? After learning how to use, how proficiently can the client perform? How pleasing is it to use its design? This falls under the class of black box testing.
 * <p>
 * Acceptance Testing
 * Acceptance testing is often done by the customer to ensure that the delivered product meets the requirements and works as the customer expected. It falls under the class of black box testing.
 * <p>
 * Regression Testing
 * Regression testing is the testing after modification of a system, component, or a group of related units to ensure that the modification is working correctly and is not damaging or imposing other modules to produce unexpected results. It falls under the class of black box testing.
 * <p>
 * Beta Testing
 * Beta testing is the testing which is done by end users, a team outside development, or publicly releasing full pre-version of the product which is known as beta version. The aim of beta testing is to cover unexpected errors. It falls under the class of black box testing.
 */
public final class TestGroupTags {

    public static final String SMOKE = "Smoke";
    public static final String REGRESSION = "Regression";
    public static final String FUNCTIONAL = "Functional";
    public static final String SANITY = "Sanity";
    public static final String INTEGRATION = "Integration";
    public static final String ACCEPTANCE = "Acceptance";

    private TestGroupTags() {
    }
}
