package com.svb.drivers;

public class MuranoDashboardElements {

    public static final String DEFAULT_HDR_FRQ_LINKS = "//h2[contains(text(),\"Frequent links\")]";

    public static final String USER_FIRST_NAME = "//div[@class=\"user-greeting ng-scope\"]";

    public static final String DEFAULT_HDR_ACCOUNT_WATCH = "//h1[@translate=\"svb.allAccounts.allAccounts\" and contains(text(),\"Account Watch\")]";

    public static final String DEFAULT_HOMEPAGE_TIPS_LNK = "//span[@translate=\"svb.customizeHomePage.tourLinkLabel\" and contains(text(),\"Homepage Tips\")]";

    public static final String DEFAULT_CUST_HOMEPAGE_TIPS_LNK = "//span[@translate=\"svb.customizeHomePage.mainLabel\" and contains(text(),\"Customize Homepage\")]";

    public static final String DEFAULT_NEXT_BTN = "//button[@class=\"mur-button pull-right next-button mur-button-next ng-binding\" and contains(text(),\"Next\")]";

    public static final String DEFAULT_HDR_ACTION_ITEMS = "//h1[@translate=\"svb.payments.actionItems\" and contains(text(),\"Action Items\")]";

    //Frequent Links Section
    public static final String DEFAULT_HDR_FRQ_LINKS_ACH = "//span[contains(text(),\"ACH\")]";

    public static final String DEFAULT_HDR_FRQ_LINKS_FX_PURCHASE = "//span[contains(text(),\"FX Purchase\")]";

    public static final String DEFAULT_HDR_FRQ_LINKS_INITIATE_SINGLE_WIRE = "//span[contains(text(),\"Initiate Single Wire\")]";

    public static final String DEFAULT_HDR_FRQ_LINKS_USER_ADMIN = "//span[contains(text(),\"User Administration\")]";

    public static final String DEFAULT_HDR_FRQ_LINKS_MANAGE_WIRE_TEMP = "//span[contains(text(),\"Manage Wire Templates\")]";

    //Murano Dashboard - Main Menu
    public static final String DEFAULT_MAIN_MENU_ACCOUNTS_LINK = "//a[@id=\"headerMenuId1\" and contains(text(),\"Accounts\")]";

    public static final String DEFAULT_MAIN_MENU_PAYMENTS_AND_TRANSFERS_LINK = "//a[@id=\"headerMenuId2\" and contains(text(),\"Payments & Transfers\")]";

    public static final String DEFAULT_MAIN_MENU_DEPOSITS_AND_RECEIVABLES_LINK = "//a[@id=\"headerMenuId3\" and contains(text(),\"Deposits & Receivables\")]";

    public static final String DEFAULT_MAIN_MENU_SERVICES_LINK = "//a[@id=\"headerMenuId4\" and contains(text(),\"Services\")]";

    public static final String DEFAULT_MAIN_MENU_UK_SERVICES_LINK = "//a[@id=\"headerMenuId0\" and contains(text(),\"UK Services\")]";

    // Common Links - Help, MainMenu, Logout
    public static final String DEFAULT_HELP_LINK = "//a[contains(text(),\"Help\")]";

    public static final String DEFAULT_MAIN_MENU_LINK = "//a[contains(text(),\"Main Menu\")]";

    public static final String DEFAULT_LOGOUT_LINK = "//a[contains(@class,\"logout-button\") and contains(text(),\"Logout\")]";

    //User Profile - Settings
    public static final String DEFAULT_PROFILE_SETTINGS_LINK = "//a[@class=\"profile-link\"]";

    public static final String DEFAULT_PROFILE_SECURITY_LINK = "//a[contains(text(),\"My Security\")]";

    public static final String DEFAULT_PROFILE_TASKS_LINK = "//a[contains(text(),\"My Tasks\")]";

    public static final String DEFAULT_PROFILE_USER_ADMIN_LINK = "//a[contains(text(),\"Users\")]";

    public static final String DEFAULT_PROFILE_AUDIT_LINK = "//a[contains(text(),\"Audits\")]";

    public static final String DEFAULT_PROFILE_AUTO_ALERTS_LINK = "//a[contains(text(),\"Automatic Alerts\")]";

    public static final String DEFAULT_PROFILE_SIGNUP_ALERTS_LINK = "//a[contains(text(),\"Sign-up Alerts\")]";

    public static final String DEFAULT_PROFILE_SECURITY_ALERTS_LINK = "//a[contains(text(),\"Security Alerts\")]";

    //Footer Links
    public static final String DEFAULT_FOOTER_SVB_LINK = "//a[@translate=\"svb.footer.svb.com\" and @href=\"http://www.svb.com/\" and contains(text(),\"SVB.com\")]";

    public static final String DEFAULT_FOOTER_LEGAL_DISCOLSURES_LINK = "//a[@translate=\"svb.footer.legalDisclosures\" and @href=\"http://www.svb.com/Privacy-Policy/\" and contains(text(),\"Legal Disclosures\")]";

    public static final String DEFAULT_FOOTER_COPYRIGHT_YEAR_LABEL = "//span[@class=\"copyright ng-binding\"]";

    public static final String DEFAULT_FOOTER_ORG_LABEL = "//span[@translate=\"svb.footer.svb\" and contains(text(),\"SVB Financial Group\") and contains(@class,\"mur-footer-margin\")]";

}
