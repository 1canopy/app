package com.svb.drivers;

import com.svb.internals.ScreenshotBookKeeper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

public abstract class BasePageSetup {

    private RemoteWebDriver webDriver;

    public BasePageSetup() {
        ScreenshotBookKeeper.clear();
    }

    public void initiateDriver() {
        webDriver = DriverManager.getDriver();
    }

    public RemoteWebDriver getDriver() {
        initiateDriver();
        return this.webDriver;
    }

    public String getPageTitle() {
        return this.webDriver.getTitle();
    }

}
