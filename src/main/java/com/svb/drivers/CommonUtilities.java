package com.svb.drivers;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class CommonUtilities {

    private long DEFAULT_TIMEOUT = 30;

    private long POLLING_TIME = 5;

    public WebElement waitForElementPresent(WebDriver driver, By byElement) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .pollingEvery(POLLING_TIME, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        WebElement webElement = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return driver.findElement(byElement);
            }
        });
        return webElement;
    }

    public boolean waitForElementToBeClickable(WebDriver driver, By byElement) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .pollingEvery(POLLING_TIME, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        WebElement webElement = wait.until(ExpectedConditions.elementToBeClickable(byElement));
        webElement.click();
        return true;
    }


    public Select selectByDropDownValue(WebDriver driver, By byElement) {
        Select dropdownElement = new Select(driver.findElement(byElement));
        return dropdownElement;
    }

    public Select selectByDropDownValueElement(WebDriver driver, WebElement webElement) {
        Select dropdownElement = new Select(webElement);
        return dropdownElement;
    }

    public static void selectCheckBox(WebElement webElementService) {
        System.out.println("");
        if (!webElementService.isSelected()) {
            webElementService.click();
        }
    }

    public static boolean isElementPresent(WebElement element) {
        try {
            if (element.isDisplayed())
                return true;
        } catch (NoSuchElementException e) {
            return false;
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    /**
     * @param driver
     * @param parentMenu
     * @param childMenu
     */
    public void onMouseHoverAction(WebDriver driver, WebElement parentMenu, WebElement childMenu) throws InterruptedException {
        Actions action = new Actions(driver);
        action.moveToElement(parentMenu).perform();
        DriverCommand.takeScreenshot("Selecting the Main menu \"" + parentMenu.getText() + "\" Page - Test Evidence");
        action.moveToElement(childMenu).click().build().perform();
    }

    /**
     * @param driver
     * @param parentMenuLinkText
     * @param childMenuLinkText
     */
    public void onMouseHoverAction(WebDriver driver, By parentMenuLinkText, By childMenuLinkText) {
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(parentMenuLinkText)).perform();
        WebElement parentMenu = driver.findElement(parentMenuLinkText);
        DriverCommand.takeScreenshot("Selecting the Main menu \"" + parentMenu.getText() + "\" Page - Test Evidence");
        action.moveToElement(driver.findElement(childMenuLinkText)).click().build().perform();
    }
}
