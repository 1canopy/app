package com.svb.core;


public class EnvironmentUtils {

    private static String envName;
    private static String appUrl;
    private static final String DEFAULT_ECONNECT_URL = "https://qa1-dfw.svbconnect.com/auth";
    private static final String DEFAULT_ECONNECT_URL_FORMATION = "https://%s.svbconnect.com/auth";

    private EnvironmentUtils(){
        //To avoid instance of this class
    }

    public static String getEnvironmentName() {
        if (null == envName) {
            String env = null;
            if (null != (env = System.getProperty("DefaultENV", "QA1"))) {
                envName = env;
            } else {
                throw new NullPointerException("The 'DefaultENV' system property was not set in maven Parameter");
            }
        }
        return envName;
    }

    public static void setAppUrl(String appUrl) {
        EnvironmentUtils.appUrl = appUrl;
    }

    public static void setEnvName(String envName) {
        EnvironmentUtils.envName = envName;
    }

    public static String getEnvHostName(String envName) {
        String tempEnvName = envName.trim();
        if (tempEnvName.equalsIgnoreCase("perf1") || tempEnvName.equalsIgnoreCase("perf2")) {
            tempEnvName = tempEnvName + ".chd";
        } else {
            tempEnvName = tempEnvName + "-dfw";
        }
        return tempEnvName;
    }
    /**
     * Get Current Url
     *
     * @return
     */
    public static String getUrl() {
        String params[] = new String[]{getEnvHostName(System.getProperty("DefaultENV","QA1"))};
        return (String.format(DEFAULT_ECONNECT_URL_FORMATION, params));
    }

}